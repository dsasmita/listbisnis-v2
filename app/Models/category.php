<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class category extends Model
{
    // protected $connection = 'mongodb';
	protected $table = 'categories';

	public static function scopeCategorySelect($query)
    {
    	$parent  = $query->select('slug','name','count','id')->whereNull('parent')
                            ->orderBy('name', 'asc')->get();
    	$arr_tmp = [];
    	$i = 0;
    	foreach ($parent as $key => $value) {
            $arr_tmp[$i]['category']['id']   = $value->id;
            $arr_tmp[$i]['category']['name'] = $value->name;
            $arr_tmp[$i]['category']['slug'] = $value->slug;
    		$arr_tmp[$i]['category']['count'] = $value->count;
    		$arr_tmp[$i]['child']    = category::select('slug','name','count','id')
                                            ->orderBy('name', 'asc')
    										->where('parent',$value->slug)->get();
    		$i++;
    	}
    	return $arr_tmp;
    }
}
