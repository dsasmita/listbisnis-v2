<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class region extends Model
{
    protected $table = 'regions';

	public static function scopeRegionSelect($query)
    {
    	$parent = $query->select('slug','name')->whereNull('parent')
                            ->orderBy('name', 'asc')->get();
    	$arr_tmp = [];
    	$i = 0;
    	foreach ($parent as $key => $value) {
            $arr_tmp[$i]['category']['name'] = $value->name;
    		$arr_tmp[$i]['category']['slug'] = $value->slug;
    		$arr_tmp[$i]['child']    = region::select('slug','name')
                                            ->orderBy('name', 'asc')
    										->where('parent',$value->slug)->get();
    		$i++;
    	}
    	return $arr_tmp;
    }
}
