<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class alamat extends Model
{
    protected $table = 'alamat';

	public function kategori()
    {
        return $this->belongsTo('App\Models\category','category','slug');
    }

    public function subKategori(){
        return $this->belongsTo('App\Models\category','category_sub','slug');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\region','provinsi','slug');
    }

    public function city(){
        return $this->belongsTo('App\Models\region','kabkota','slug');
    }

    public function pesan(){
        return $this->belongsTo('App\Models\commentRating','id','id_lokasi');
    }
}
