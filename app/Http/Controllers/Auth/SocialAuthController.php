<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use Session;
use Auth;

use App\User;

class SocialAuthController extends Controller
{
    public function facebookRedirect()
    {
    	return Socialite::driver('facebook')->redirect();
    }

    public function facebookCallback()
    {
    	$user  = Socialite::driver('facebook')->user();
        $fbUser = $user->user;
        $checkUser = User::where('email',$user->email)->first();
        
        if($checkUser){
            $checkUser->login_provider  = 'facebook';
            $checkUser->facebook_id = $fbUser['id'];
            $checkUser->save();
        }else{
            $checkUser          = new User();
            $checkUser->name    = $fbUser['name'];
            $checkUser->email   = $fbUser['email'];
            $checkUser->gender  = $fbUser['gender'];
            $checkUser->status  = 'verified';
            $checkUser->login_provider  = 'facebook';
            $checkUser->link_facebook = $fbUser['link'];
            $checkUser->facebook_id = $fbUser['id'];
            $checkUser->save();
        }

        Auth::guard('user')->login($checkUser);
        Session::flash('notif-success', 'Proses login berhasil');
        return redirect(route('user.home'));
    }

    public function twitterRedirect()
    {
        return Socialite::driver('twitter')->redirect();
    }

    public function twitterCallback()
    {
        $user  = Socialite::driver('twitter')->user();

        $checkUser = User::where('email',$user->email)->first();
        
        if($checkUser){
            $checkUser->login_provider  = 'twitter';
            $checkUser->twitter_id = $user->id;
            $checkUser->save();
        }else{
            $checkUser          = new User();
            $checkUser->name    = $user->name;
            $checkUser->email   = $user->email;
            $checkUser->status  = 'verified';
            $checkUser->login_provider  = 'twitter';
            $checkUser->twitter_id = $user->id;
            $checkUser->save();
        }

        Auth::guard('user')->login($checkUser);
        Session::flash('notif-success', 'Proses login berhasil');
        return redirect(route('user.home'));
    }

    public function googleRedirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback()
    {
        $user  = Socialite::driver('google')->user();

        $checkUser = User::where('email',$user->email)->first();
        
        if($checkUser){
            $checkUser->login_provider  = 'google';
            $checkUser->google_id = $user->id;
            $checkUser->save();
        }else{
            $checkUser          = new User();
            $checkUser->name    = $user->name;
            $checkUser->email   = $user->email;
            $checkUser->status  = 'verified';
            $checkUser->login_provider  = 'google';
            $checkUser->google_id = $user->id;
            $checkUser->save();
        }

        Auth::guard('user')->login($checkUser);
        Session::flash('notif-success', 'Proses login berhasil');
        return redirect(route('user.home'));
    }
}
