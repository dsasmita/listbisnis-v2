<?php

namespace App\Http\Controllers\Auth;

use App\User;

use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Session;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/member';
    protected $guard = 'user';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    public function showLoginForm(Request $request){
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.png');

        $arr_data['title']      = 'Login | '.$this->title;
        return view('user.auth.login', $arr_data);
    }

    public function showRegistrationForm(Request $request){
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.png');

        $arr_data['title']      = 'Login | '.$this->title;
        return view('user.auth.register',$arr_data);
    }

    public function showResetForm(){
        return view('user.auth.register');
    }

    public function loginPost(Request $request) {
        $login = $request->get('login');
        $password = $request->get('password');

        $field = null;

        if (filter_var($request->get('login'), FILTER_VALIDATE_EMAIL)) {
            $field = 'email';
        } else {
            $field = 'username';
        }

        try {
            $credentials = [
                $field => $login,
                'password' => $password
            ];

            $throttles = $this->isUsingThrottlesLoginsTrait();

            if ($throttles && $this->hasTooManyLoginAttempts($request)) {
                return $this->sendLockoutResponse($request);
            }

            if (Auth::guard('user')->attempt($credentials, $request->has('remember'))) {
                return $this->handleUserWasAuthenticated($request, $throttles);
            }
            //dd($this->getFailedLoginMessage());
            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            if ($throttles) {
                $this->incrementLoginAttempts($request);
            }

            return redirect()->route('user.login')->withInput()->withErrors([$this->getFailedLoginMessage()]);
        } catch (\Exception $e) {
            return redirect()->route('user.login')->withInput()->withErrors(['Check input']);
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'username'  => 'required|alpha_num|max:60|unique:users',
            'password'  => 'required|min:6|confirmed',
            'tos'       => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'username'  => $data['username'],
            'password'  => bcrypt($data['password']),
        ]);
    }
}
