<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\alamat;

class bisnisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index(Request $request){
    	$data 		  = [];
    	$data['list'] = alamat::orderBy('views','desc')->paginate($this->limit);
        return view('admin.bisnis.index',$data);
    }
}
