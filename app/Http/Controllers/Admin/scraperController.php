<?php

namespace App\Http\Controllers\Admin;

/**
 * scraper class
 *
 * @package scraperController
 * @author dangs.work@gmail.com
 **/

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Sunra\PhpSimple\HtmlDomParser;
use App\Models\category;
use App\Models\region;
use App\Models\alamat;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/

class scraperController extends Controller
{

	public function normalisasiKoordinat()
	{
		// $response = \GoogleMaps::load('geocoding')
		// 			        ->setParamByKey('latlng','-6.2275903631982, 106.79764347413')
		// 			        ->get('results');
		// dd($response);

		// Central Jakarta City

		$bisnis = alamat::whereNull('kabkota')->whereNotNull('latitude')->limit(250)->get();
		
		$i = 1;
		foreach ($bisnis as $bis) {
			$response = \GoogleMaps::load('geocoding')
					        ->setParamByKey('latlng',trim($bis->latitude).','.trim($bis->longitude))
					        ->get('results.address_components');

			$address = [];
			if(@$response['results'][0]){
				foreach ($response['results'][0]['address_components'] as $add) {
					if($add['types'][0] == 'postal_code'){
						@$address['postal_code'] = $add['long_name'];
					}
					if($add['types'][0] == 'administrative_area_level_2'){
						@$address['kabkota'] = trim(str_replace(['Kabupaten','Kota'], '', $add['long_name']));
						@$address['kabkota_long'] = trim($add['long_name']);
					}
					if($add['types'][0] == 'administrative_area_level_1'){
						@$address['provinsi'] = $add['long_name'];
					}
					if($add['types'][0] == 'country'){
						@$address['negara'] = $add['long_name'];
					}
				}
			}

			if(trim(@$address['kabkota']) == 'Central Jakarta City'){
				$slug = 'jakarta-pusat';
			}else if(trim(@$address['kabkota']) == 'South Jakarta City'){
				$slug = 'jakarta-selatan';
			}else if(trim(@$address['kabkota']) == 'East Jakarta City'){
				$slug = 'jakarta-timur';
			}else{
				$slug = str_slug(trim(@$address['kabkota']));
			}

			$kabkota = region::where('slug',$slug)->first();
			if($kabkota){
				$bis->kabkota  = $kabkota->slug;
				$bis->provinsi = $kabkota->parent;
				$bis->kodepos  = @$address['postal_code'];
				$bis->address  = $bis->address .' - '.implode(', ', $address);
				$bis->save();
			}else{
				$slug    = str_slug(@$address['kabkota_long']);
				$kabkota = region::where('slug',$slug)->first();
				if($kabkota){
					$bis->kabkota  = $kabkota->slug;
					$bis->provinsi = $kabkota->parent;
					$bis->kodepos  = @$address['postal_code'];
					$bis->address  = $bis->address .' - '.implode(', ', $address);
					$bis->save();
				}
			}
			echo $i.') '.'('.$bis->latitude.','.$bis->longitude.')'.implode('##', $address).'<br>';
			$i++;
		}
		return 'test';
	}

	public function alamtTerbaru()
	{
		$html 	= HtmlDomParser::file_get_html('http://alamatku.detik.com/maps');
		$check = @$html->find('.equalize li h3 a',0)->innertext;
		if($check){
			foreach ($html->find('.equalize li h3 a') as $e) {
				$slug = explode('/', $e->href);
				$check_alamat = alamat::where('slug',$slug[2])->first();
				$link = 'http://alamatku.detik.com'.trim($e->href);
				
				if(!$check_alamat){
					echo 'Link '.$e->href.' belum ada<br>';
				}else{
					echo 'Link '.$e->href.' sudah ada<br>';
				}
			}
		}
		return 'alamat terbaru';
	}

	public function kategoriNormalisasi(){
		foreach(\App\Models\category::categorySelect() as $select)
		{
			$category = category::find($select['category']['id']);
			$category->count = alamat::where('category',$category->slug)->count();
			$category->save();
			foreach($select['child'] as $selectChild)
			{
				$selectChild->count = alamat::where('category_sub',$selectChild->slug)->count();
				$selectChild->save();
			}
		}
		return 'normalisasi';
	}

	public function viewKategori(){
		$category = category::whereNotNull('parent')->orderBy('name','asc')->get();
		foreach ($category as $cat) {
			echo '<a href="'.route('scrap.kategori.move', $cat->slug).'">'.$cat->name."</a><br>";
		}
		return '<hr>true';
	}

	public function kategoriMove($id){
		$category = category::where('slug',$id)->first();
		// $i=439; //atm-bri 
		// $i=0;  //amerika
		// $i = 83; //asia
		// $i = 127; //bakery
		$i = 0;
		$check = 'lanjut';
		ini_set('max_execution_time', 7200);
		do{
			$i++;
			// $link = 'http://alamatku.detik.com/kategori/otomotif/subkategori/ban-spooring-&-balancing';
			$link = $category->link_alamatku;
			$check = $this->kategoriDetail($link.'?page='.$i.'&limit=20');
			echo $i.') '.$check.'<br>';
		}while ( $check != 'stop' );
		return '<hr>ending';
	}
	
	public function kategori()
	{
		$html = HtmlDomParser::file_get_html('http://alamatku.detik.com/kategori');

		$i = 1;
		foreach($html->find('li.boxy') as $e){
			
			$subLink = 'http://alamatku.detik.com'.@$e->find('.small',0)->href;
			$title = $e->find('.title',0)->innertext;
			$slug = str_slug(str_replace('&amp;', '', $title));

			$category 					= category::where('slug', $slug)->first();
			if(!$category){
				$category					= new category();
				$category->name 			= $title;
				$category->slug 			= $slug;
				$category->link_alamatku 	= $subLink;
				$category->save();
			}

			echo $i.'). '.$e->find('.title',0)->innertext . ' ## '.$subLink.'<br>';
			$kategoriHtml = HtmlDomParser::file_get_html($subLink);
			
			foreach ($kategoriHtml->find('.boxy') as $f) {
				foreach ($f->find('li a') as $g) {
					echo ">>".$g->innertext.'<br>';
					echo "==>>".$g->href.'<br>';

					$subSubLink = 'http://alamatku.detik.com'.@$g->href;
					$subTitle = $g->innertext;
					$subSlug = str_slug(str_replace('&amp;', '', $subTitle));

					$subCategory 					= category::where('slug', $subSlug)->first();
					if(!$subCategory){
						$subCategory				= new category();
						$subCategory->name 			= $subTitle;
						$subCategory->parent 		= $slug;
						$subCategory->slug 			= $subSlug;
						$subCategory->link_alamatku = $subSubLink;
						$subCategory->save();
					}
				}
			}
			echo "<br>";
			$i++;
			$kategoriHtml->clear();
		}
		$html->clear();
		return 'kategori';
	}

	public function regions()
	{
		$html = HtmlDomParser::file_get_html('http://alamatku.detik.com/area');

		$i = 1;
		foreach($html->find('li.boxy') as $e){
			
			$link = 'http://alamatku.detik.com'.@$e->find('.title a',0)->href;
			$title = str_replace('&amp;', 'dan', $e->find('.title a',0)->innertext);
			$slug = str_slug(str_replace('dan', '', $title));

			$category 					= region::where('slug', $slug)->first();
			if(!$category){
				$category					= new region();
				$category->name 			= $title;
				$category->slug 			= $slug;
				$category->link_alamatku 	= $link;
				$category->save();
			}

			$moreLink = 'http://alamatku.detik.com'.@$e->find('.small',0)->href;

			echo $i.'). '.$title . ' ## '.$link.'<br>';
			if($moreLink != 'http://alamatku.detik.com'){
				$kategoriHtml = HtmlDomParser::file_get_html($moreLink);
				foreach ($kategoriHtml->find('.boxy') as $f) {
					foreach ($f->find('li a') as $g) {
						
						$subSubLink = 'http://alamatku.detik.com'.@$g->href;
						$subTitle = str_replace('&amp;', 'dan', $g->innertext);
						$subSlug = str_slug(str_replace('dan', '', $subTitle));

						$subCategory 					= region::where('slug', $subSlug)->first();
						if(!$subCategory){
							$subCategory				= new region();
							$subCategory->name 			= $subTitle;
							$subCategory->parent 		= $slug;
							$subCategory->slug 			= $subSlug;
							$subCategory->link_alamatku = $subSubLink;
							$subCategory->save();
						}

						echo ">>".$subTitle.'<br>';
						echo "==>>".$subSubLink.'<br>';
					}
				}
				$kategoriHtml->clear();
			}else{
				foreach ($e->find('li') as $f) {
					foreach ($f->find('a') as $g) {
						$subSubLink = 'http://alamatku.detik.com'.@$g->href;
						$subTitle = str_replace('&amp;', 'dan', $g->innertext);
						$subSlug = str_slug(str_replace('dan', '', $subTitle));

						$subCategory 					= region::where('slug', $subSlug)->first();
						if(!$subCategory){
							$subCategory				= new region();
							$subCategory->name 			= $subTitle;
							$subCategory->parent 		= $slug;
							$subCategory->slug 			= $subSlug;
							$subCategory->link_alamatku = $subSubLink;
							$subCategory->save();
						}

						echo ">>".$subTitle.'<br>';
						echo "==>>".$subSubLink.'<br>';
					}
				}
			}
			echo "<br>";
			$i++;
		}
		$html->clear();
		return 'kategori';
	}

	public function detail()
	{
		$html 	= HtmlDomParser::file_get_html('http://alamatku.detik.com/direktori/atm-citibank-metropolitan-mall');

		$title 	= $html->find('h3',0)->plaintext;
		echo 'Title: '.$title.'<br>';

		$kategori = $html->find('.breadcrumb li a',0)->innertext;
		echo 'Kategori: '.$kategori.'<br>';

		$subKategori = $html->find('.breadcrumb li a',1)->innertext;
		echo 'Sub Kategori: '.$subKategori.'<br><hr>';
		
		$containerDetail = $html->find('.detail',0)->innertext;
		echo $containerDetail.'<br>';
		return '<br>====';
	}

	public function kategoriDetail($link)
	{
		$html 	= HtmlDomParser::file_get_html($link);
		$check = @$html->find('.box_2 ul li',0)->innertext;
		if($check){
			foreach($html->find('.box_2 ul li') as $e){
				$slug = explode('/', $e->find('h3 a',0)->href);
				$check_alamat = alamat::where('slug',$slug[2])->first();
				$link = 'http://alamatku.detik.com'.trim($e->find('h3 a',0)->href);
				$nama = $e->find('h3 a',0)->innertext;
				
				$blackList = [
					'http://alamatku.detik.com/direktori/vinta-caf',
					'http://alamatku.detik.com/direktori/pppa-darul-qur-an',
					'http://alamatku.detik.com/direktori/coffee-toffee-2',
					'http://alamatku.detik.com/direktori/rochor-beancurd-house-balestier-branch',
					'http://alamatku.detik.com/direktori/puskopad-a-kodam-iii-siliwangi',
					'http://alamatku.detik.com/direktori/langgar-fatchul-mu-min',
					'http://alamatku.detik.com/direktori/masjid-ja-far-al-basyir-bangunharjo',
					'http://alamatku.detik.com/direktori/masjid-jami-kodama',
					'http://alamatku.detik.com/direktori/swensen-s-cafe-restaurant',
					'http://alamatku.detik.com/direktori/tom-s-palette',
					'http://alamatku.detik.com/direktori/ben---jerry-s',
					'http://alamatku.detik.com/direktori/andersen-s-ice-cream',
					'http://alamatku.detik.com/direktori/black-canyon-caf-cipete',
					'http://alamatku.detik.com/direktori/i-box',
					'http://alamatku.detik.com/direktori/gloria-jean-s-coffee',
					'http://alamatku.detik.com/direktori/nu-art-caf-2',
					'http://alamatku.detik.com/direktori/mad-jack-cafe-pasir-ris-branch',
					'http://alamatku.detik.com/direktori/omodachi-caf',
					'http://alamatku.detik.com/direktori/omodachi-caf',
					'http://alamatku.detik.com/direktori/gianni-s-2',
					'http://alamatku.detik.com/direktori/bubur-ayam-wahyoe',
					'http://alamatku.detik.com/direktori/chik-s-advertising',
					'http://alamatku.detik.com/direktori/atm-bni-syari-ah-fatmawati',
					'http://alamatku.detik.com/direktori/atm-hsbc-plaza-indonesia-%EF%BF%BD%EF%BF%BD-ex',
					'http://alamatku.detik.com/direktori/atm-nisp-d&amp;amp;#39;best-fatmawati',
					'http://alamatku.detik.com/direktori/atm-nisp-d&amp;#39;best-fatmawati',
					'http://alamatku.detik.com/direktori/depo-air-minum-air-minum-sehat',
					'http://alamatku.detik.com/direktori/akdemi-bahasa-asing-aba-yipk',
					'http://alamatku.detik.com/direktori/sekolah-transportasi-udara-dan-pramugari-iiam',
					'http://alamatku.detik.com/direktori/steie-st-pagnatelli-karang-asem-laweyan',
					'http://alamatku.detik.com/direktori/stranough--guitar---technology',
					'http://alamatku.detik.com/direktori/chili-s-grill---bar',
					'http://alamatku.detik.com/direktori/o-learys-sportsbar---grill',
					'http://alamatku.detik.com/direktori/sewa-mobil-box---0852-1746-4000----021-333-76-345-3',
					'http://alamatku.detik.com/direktori/kramat-djati-po-agen-malang-%E2%80%93-ade-irma-suryani',
					'http://alamatku.detik.com/direktori/apartemen-keraton-u/c-2009)',
					'http://alamatku.detik.com/direktori/hobiku-%EF%BF%BD%EF%BF%BDikan',
					'http://alamatku.detik.com/direktori/chuan-jiang-hao-zi-steamboat-restaurant---chinatown-branch',
					'http://alamatku.detik.com/direktori/ayam-goreng-lombok-idjo-1',
					'http://alamatku.detik.com/direktori/pondok-ayam-suyatik-empe',
					'http://alamatku.detik.com/direktori/ayam-bakar-wong-solo-cab-abdullah-syafi&amp;#39;ie',
					'http://alamatku.detik.com/direktori/bebek---ayam-kremes-i&amp;#39;m-mirasa',
					'http://alamatku.detik.com/direktori/bakmi-pandanaran-restaurant---linda-bakery-cookies-ice-cream',
					'http://alamatku.detik.com/direktori/gianni-s-1',
					'http://alamatku.detik.com/direktori/the-flyin--bread',
					'http://alamatku.detik.com/direktori/mrs-field-s-cookies',
					'http://alamatku.detik.com/direktori/chocolat-n-spice',
					'http://alamatku.detik.com/direktori/beard-papa-s',
					'http://alamatku.detik.com/direktori/madeleine-s-original-portuguese-egg-tart',
					'http://alamatku.detik.com/direktori/baker-s-well',
					'http://alamatku.detik.com/direktori/bom-bom-bread-bogor',
					'http://alamatku.detik.com/direktori/bakso-botak',
					'http://alamatku.detik.com/direktori/bakso-pak-i-ang',
					'http://alamatku.detik.com/direktori/bakso-kepak-sapi-ma-kress',
					'http://alamatku.detik.com/direktori/bank-rakyat-indonesia-bri)-syari&amp;#39;ah-cabang-plaza-pondok-gede',
					'http://alamatku.detik.com/direktori/bank-rakyat-indonesia-bri)-syari&amp;#39;ah-tanah-abang',
					'http://alamatku.detik.com/direktori/muddy-murphy-s-irish-pub',
					'http://alamatku.detik.com/direktori/baen-dha-batik/jilbab)',
					'http://alamatku.detik.com/direktori/bengkel-tralis---sugiari-senangar',
					'http://alamatku.detik.com/direktori/john-s-cleaners',
					'http://alamatku.detik.com/direktori/5a-sec-dry-cleaning-1',
					'http://alamatku.detik.com/direktori/dia-s-laundry'
				];

				if(!$check_alamat && !in_array($link, $blackList ) && !strpos($link, 'syari-ah') && !strpos($link, '&amp;')){
					$nama = $e->find('h3 a',0)->innertext;
					$alamat = $e->find('.alamat',0)->innertext;

					$detailPage = HtmlDomParser::file_get_html($link);
					if($detailPage){
						//get kategori and sub kategori
						$kategori = $detailPage->find('.breadcrumb li a',0)->innertext;
						$cat      = category::where('name',trim($kategori))->first();

						$subKategori = $detailPage->find('.breadcrumb li a',1)->innertext;
						$subCat      = category::where('name',trim($subKategori))->first();

						//get coord
						$latitude = 0;
						$longitude = 0;
						$text =  $detailPage->innertext;
						$split = explode('myLatlng = new google.maps.LatLng', $text);
						if(count($split) >= 2 ){
							$split2 = explode(';', $split[1]);
							$result = $split2[0];
							$result = str_replace('(','',$result);
							$result = str_replace(')','',$result);
							$result = explode(',', $result);
							if(count($result) == 2){
								$latitude = $result[0];
								$longitude = $result[1];
							}
						}

						// logo
						$logo = $detailPage->find('.logo_detail img',0)->src;
						$logoLocation = '';
						if($logo != '/assets/def_img1.jpg' && $logo != '/assets/def_img2.jpg'){
							$logo = str_replace('thumb', 'original', $logo);
							$ch = curl_init($logo);
							$name = explode('/', $logo);
							$count =  count($name)-1;

							$base_url = 'uploads/'.date('Y/m/d/');
							$exist = file_exists(env('BASE_UPLOAD').$base_url);
							if(!$exist){
								mkdir($base_url, 0777, true);
							}
							$fileExist = file_exists(env('BASE_UPLOAD').$base_url.$name[$count]);
							if(!$fileExist){
								$fp = fopen($base_url.$name[$count], 'wb');
								curl_setopt($ch, CURLOPT_FILE, $fp);
								curl_setopt($ch, CURLOPT_HEADER, 0);
								curl_exec($ch);
								curl_close($ch);
								fclose($fp);
							}
							$logoLocation = $base_url.$name[$count];
						}
						if($alamat != ''){
							$alamatInsert = new alamat();
							$alamatInsert->name = $nama;
							$alamatInsert->slug = $slug[2];
							$alamatInsert->link_alamatku = $link;
							$alamatInsert->address = $alamat;
							$alamatInsert->category = $cat->slug;
							$alamatInsert->category_sub = $subCat->slug;
							if($latitude != 0 && $longitude != 0){
								$alamatInsert->latitude = $latitude;
								$alamatInsert->longitude = $longitude;
							}
							if($logoLocation != '')
								$alamatInsert->logo = $logoLocation;
							$alamatInsert->save();
						}
					}
				}
			}
			return 'lanjut';
		}else{
			return 'stop';
		}
	}

	public function kategoriDetailEcho($link)
	{
		$html 	= HtmlDomParser::file_get_html($link);
		$check = @$html->find('.box_2 ul li',0)->innertext;
		if($check){
			foreach($html->find('.box_2 ul li') as $e){
				$link = 'http://alamatku.detik.com'.$e->find('h3 a',0)->href;
				$nama = $e->find('h3 a',0)->innertext;
				$alamat = $e->find('.alamat',0)->innertext;
				
				echo 'nama: '.$nama."<br>";
				echo 'link: '.$link."<br>";
				echo 'alamat: '.$alamat."<br>";

				$detailPage = HtmlDomParser::file_get_html($link);
				
				//get kategori and sub kategori
				$kategori = $detailPage->find('.breadcrumb li a',0)->innertext;
				$cat      = category::where('name',trim($kategori))->first();
				echo $cat->slug.'<br>';
				

				$subKategori = $detailPage->find('.breadcrumb li a',1)->innertext;
				$subCat      = category::where('name',trim($subKategori))->first();
				echo @$subCat->slug.'<br>';

				//get coord
				$latitude = 0;
				$longitude = 0;
				$text =  $detailPage->innertext;
				$split = explode('myLatlng = new google.maps.LatLng', $text);
				if(count($split) >= 2 ){
					$split2 = explode(';', $split[1]);
					$result = $split2[0];
					$result = str_replace('(','',$result);
					$result = str_replace(')','',$result);
					$result = explode(',', $result);
					if(count($result) == 2){
						$latitude = $result[0];
						$longitude = $result[1];
					}
				}
				echo "($latitude,$longitude)<br>";

				// logo
				$logo = $detailPage->find('.logo_detail img',0)->src;
				if($logo != '/assets/def_img1.jpg' || $logo != '/assets/def_img2.jpg'){
					$logo = str_replace('thumb', 'original', $logo);
					$ch = curl_init($logo);
					$name = explode('/', $logo);
					$count =  count($name)-1;

					$base_url = env('BASE_UPLOAD').'/'.date('Y/m/d/');
					$exist = file_exists($base_url);
					if(!$exist){
						mkdir($base_url, 0777, true);
					}
					$fileExist = file_exists($base_url.$name[$count]);
					if(!$fileExist){
						$fp = fopen($base_url.$name[$count], 'wb');
						curl_setopt($ch, CURLOPT_FILE, $fp);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_exec($ch);
						curl_close($ch);
						fclose($fp);
					}
					echo $logo.'<br>';
				}
				echo '<hr>';
			}
			return 'lanjut';
		}else{
			return 'stop';
		}
	}
}