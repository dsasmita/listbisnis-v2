<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\region;

use App\Models\mysql\region as regionMySql;
use App\Models\region as regionMongo;

use App\Models\mysql\category as categoryMySql;
use App\Models\category as categoryMongo;

use App\Models\mysql\alamat as alamatMySql;
use App\Models\alamat as alamatMongo;

class migrateToMysqlController extends Controller
{
    public function region(Request $request)
    {
    	$mongo = regionMongo::get();
    	foreach ($mongo as $key => $m) {
    		$mysql                = regionMySql::where('slug',$m->slug)->first();
    		if(!$mysql){
	    		$mysql                = new regionMySql();
	    		$mysql->name          = $m->name;
	    		$mysql->slug          = $m->slug;
	    		$mysql->parent        = $m->parent;
	    		$mysql->link_alamatku = $m->link_alamatku;
	    		$mysql->save();
    		}
    	}
    	return 'region';
    }

    public function category(Request $request)
    {
    	$mongo = categoryMongo::get();
    	foreach ($mongo as $key => $m) {
    		$mysql                = regionMySql::where('slug',$m->slug)->first();
    		if(!$mysql){
	    		$mysql                = new categoryMySql();
	    		$mysql->name          = $m->name;
	    		$mysql->slug          = $m->slug;
	    		$mysql->parent        = $m->parent;
	    		$mysql->link_alamatku = $m->link_alamatku;
	    		$mysql->save();
    		}
    	}
    	return 'category';
    }

    public function alamat(Request $request)
    {
    	ini_set('memory_limit', '-1');
    	ini_set('max_execution_time', 3000);
  //   	alamatMongo::offset(99000)->limit(5000)->chunk(500, function ($mongos) {
		//     foreach ($mongos as $m) {
		//         $mysql                = alamatMySql::where('slug',$m->slug)->first();
	 //    		if(!$mysql){
		//     		$mysql                = new alamatMySql();
		//     		$mysql->name          = $m->name;
		//     		$mysql->slug          = $m->slug;
		//     		$mysql->address       = $m->address;
		//     		$mysql->category      = $m->category;
		//     		$mysql->category_sub  = $m->category_sub;
		//     		$mysql->latitude      = $m->latitude;
		//     		$mysql->longitude     = $m->longitude;
		//     		$mysql->link_alamatku = $m->link_alamatku;
		//     		$mysql->save();
	 //    		}
		//     }
		// });
		$mongo = alamatMongo::offset(99497)->limit(5000)->orderBy('created_at','desc')->get();
    	foreach ($mongo as $key => $m) {
    		$mysql                = alamatMySql::where('slug',$m->slug)->first();
    		if(!$mysql){
	    		$mysql                = new alamatMySql();
	    		$mysql->name          = $m->name;
	    		$mysql->slug          = $m->slug;
	    		$mysql->address       = $m->address;
	    		$mysql->category      = $m->category;
	    		$mysql->category_sub  = $m->category_sub;
	    		$mysql->latitude      = $m->latitude;
	    		$mysql->longitude     = $m->longitude;
	    		$mysql->link_alamatku = $m->link_alamatku;
	    		$mysql->save();
    		}
    	}
    	return 'alamat';
    }
}
