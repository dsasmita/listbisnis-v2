<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use Validator;
use Session;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;

class profileController extends Controller
{
    public function index(Request $request){
    	$arr_data 		 = [];
    	$arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url']   = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.png');

        $arr_data['profil'] = \Auth::guard('user')->user();

        $arr_data['title'] = 'Profile | '.$this->title;
        return view('user.profile.index',$arr_data);
    }

    public function updateProfile(Request $request)
    {
    	$input = $request->all();
        $validator = Validator::make(
                        $input, [
                        		'name'   => 'required',
                        		'gender' => 'required',

                            ]
        );

        Session::flash('profile-action', 'profile');
        if ($validator->fails()) {
            return redirect(route('user.profile'))
                            ->withErrors($validator)->withInput();
        }

        $profile = User::find(\Auth::guard('user')->user()->id);
        $profile->name   = $request->input('name');
        $profile->gender = $request->input('gender');
        $profile->save();

        Session::flash('notif-success', 'Ubah data profil berhasil');
        return redirect(route('user.profile'));
    }

    public function updatePassword(Request $request)
    {
        $profile = \Auth::guard('user')->user();
        if($profile->password != ''){
        	$validator = Validator::make(
                        $request->all(), [
                        		'password_old' => 'required',
                        		'password'     => 'required|min:6|confirmed',
                            ]);
        }else{
        	$validator = Validator::make(
                        $request->all(), [
                        		'password'     => 'required|min:6|confirmed',
                            ]);
        }

        Session::flash('profile-action', 'password');
        if ($validator->fails()) {
            return redirect(route('user.profile'))
                            ->withErrors($validator)->withInput();
        }

        if($profile->password != '')
        {
        	if (!Hash::check($request->input('password_old'), $profile->password))
        	{
        		return 'password tidak ditemukan';
        	}
        }

        $profile->password = bcrypt($request->input('password'));
        $profile->save();

    	Session::flash('notif-success', 'Ubah password berhasil');
        return redirect(route('user.profile'));
    }
}
