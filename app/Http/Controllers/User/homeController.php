<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class homeController extends Controller
{
    public function index(Request $request){
    	$arr_data 		 = [];
    	$arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url']   = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.png');

        $arr_data['title']      = 'Dashboard | '.$this->title;
        return view('user.home.index',$arr_data);
    }
}
