<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use Validator;
use Session;
use Input;
use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\alamat;
use App\Models\category;
use App\Models\region;

class bisnisController extends Controller
{
    public function index(Request $request){
    	$arr_data 		 = [];
    	$arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url']   = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.png');

        $arr_data['list'] = alamat::where('claim_by',\Auth::guard('user')->user()->id)
        						->orderBy('updated_at','desc')->paginate($this->limit);

        $arr_data['title']      = 'Bisnis | '.$this->title;
        return view('user.bisnis.index',$arr_data);
    }

    public function create(Request $request){
        $update = [];
        // info
        $update['name']         = old('name');
        $update['category']     = old('category');
        $update['category_sub'] = old('category_sub');
        $update['description']  = old('description');
        if(old('latitude') == ''){
            $update['latitude'] = env('DEFAULT_LATITUDE');
        }else{
            $update['latitude'] = old('latitude');
        }
        if(old('longitude') == ''){
            $update['longitude'] = env('DEFAULT_LONGITUDE');
        }else{
            $update['longitude'] = old('longitude');
        }
        // alamat
        $update['provinsi'] = old('provinsi');
        $update['kabkota']  = old('kabkota');
        $update['kodepos']  = old('kodepos');
        $update['address']  = old('address');
        // kontak
        $update['phone_number']   = old('phone_number');
        $update['fax_number']     = old('fax_number');
        $update['email']          = old('email');
        $update['website']        = old('website');
        $update['fb_link']        = old('fb_link');
        $update['twitter_link']   = old('twitter_link');
        $update['instagram_link'] = old('instagram_link');
        // SEO
        $update['seo_title']       = old('seo_title');
        $update['seo_keyword']     = old('seo_keyword');
        $update['seo_description'] = old('seo_description');
        $arr_data['update']        = $update;

        // select
        $arr_data['categories'] = category::select('slug','name','count','id')
                                    ->whereNull('parent')->orderBy('name','asc')->get();
        $arr_data['categories_sub'] = category::select('slug','name','count','id')
                                    ->whereNotNull('parent')->where('parent',@$update['category'])->orderBy('name','asc')->get();
        $arr_data['provinsies'] = region::select('slug','name','id')
                                    ->whereNull('parent')->orderBy('name','asc')->get();
        $arr_data['kabkotas']   = region::select('slug','name','id')
                                    ->whereNotNull('parent')->where('parent',@$update['provinsi'])->orderBy('name','asc')->get();
        // end select

        $arr_data['title']  = 'Tambah Bisnis Baru | '.$this->title;
        return view('user.bisnis.create',$arr_data);
    }

    public function createPost(Request $request){
        $input = $request->all();
        $validator = Validator::make(
                        $input, [
                                'latitude' => 'required|numeric',
                                'longitude'=> 'required|numeric',
                                'name'     => 'required',
                                'category' => 'required',
                                'description' => 'required',
                                'provinsi' => 'required',
                                'kabkota'  => 'required',
                                'kodepos'  => 'required|numeric|digits:5',
                                'address'  => 'required',
                                'phone_number' => 'required|numeric',
                                'fax_number' => 'numeric',
                                'email'    => 'email',
                                'website'  => 'url',
                                'fb_link'  => 'url',
                                'twitter_link'  => 'url',
                                'instagram_link'=> 'url',
                                'file_logo'     => 'image|max:512'
                                ]
        );

        if ($validator->fails()) {
            return redirect(route('user.bisnis.create'))
                            ->withErrors($validator)->withInput();
        }

        $bisnis           = new alamat();
        $bisnis->claim_by = \Auth::guard('user')->user()->id;
        // maps
        $bisnis->latitude  = $request->latitude;
        $bisnis->longitude = $request->longitude;
        // info
        // slug
        $slug  = str_slug($request->input('name'));
        $finalSlug = $slug;
        $check = alamat::where('slug',$slug)->first();
        $i = 1;
        while ($check) {
            $check = alamat::where('slug',$slug.'-'.$i)->first();
            if(!$check){
                $finalSlug = $slug.'-'.$i;
                break;
            }
            $i++;
        }
        // end slug
        $bisnis->slug         = $finalSlug; 
        $bisnis->name         = $request->input('name');
        $bisnis->category     = $request->input('category');
        $bisnis->category_sub = $request->input('category_sub');
        $bisnis->description  = $request->input('description');
        
        // alamat
        $bisnis->provinsi = $request->input('provinsi');
        $bisnis->kabkota  = $request->input('kabkota');
        $bisnis->kodepos  = $request->input('kodepos');
        $bisnis->address  = $request->input('address');
        // kontak
        $bisnis->phone_number   = $request->input('phone_number');
        $bisnis->fax_number     = $request->input('fax_number');
        $bisnis->email          = $request->input('email');
        $bisnis->website        = $request->input('website');
        $bisnis->fb_link        = $request->input('fb_link');
        $bisnis->twitter_link   = $request->input('twitter_link');
        $bisnis->instagram_link = $request->input('instagram_link');
        // SEO
        $bisnis->seo_title       = $request->input('seo_title');
        $bisnis->seo_keyword     = $request->input('seo_keyword');
        $bisnis->seo_description = $request->input('seo_description');

        if($request->file('file_logo') && $request->file('file_logo')->isValid()){
            $logo     = $request->file('file_logo');
            $base_url = 'uploads/'.date('Y/m/d/');

            $exist = file_exists($base_url);
            if(!$exist){
                mkdir($base_url, 0777, true);
            }

            $extension = $logo->getClientOriginalExtension();
            $fileNameOri  = 'logo-' . \Auth::guard('user')->user()->id . '.' . $extension;
            $fileNameSquare  = 'square-logo-' . \Auth::guard('user')->user()->id . '.' . $extension;
            $logo->move($base_url, $fileNameOri);
            
            Image::make($base_url . $fileNameOri)->widen(400)->save($base_url . $fileNameSquare);
            Image::make($base_url . $fileNameSquare)->resizeCanvas(400,400)->save();

            $bisnis->logo = $base_url . $fileNameOri;
            $bisnis->logo_filename = $fileNameOri;
        }
        $bisnis->save();

        Session::flash('notif-success', 'Tambah detail bisnis '.$bisnis->name.' berhasil.');
        return redirect(route('user.bisnis'));
    }

    public function detail($id, Request $request){
    	$bisnis = alamat::where('claim_by',\Auth::guard('user')->user()->id)
    					->where('id',$id)->first();
    	if(!$bisnis){
    		\Session::flash('notif-error', 'Data bisnis tidak ditemukan');
            return redirect(route('user.bisnis'));
    	}
    	$arr_data['bisnis'] = $bisnis;

        $update = [];
        // info
        if(old('name') == ''){
            $update['name'] = $bisnis->name;
        }else{
            $update['name'] = old('name');
        }
        if(old('category') == ''){
            $update['category'] = $bisnis->category;
        }else{
            $update['category'] = old('category');
        }
        if(old('category_sub') == ''){
            $update['category_sub'] = $bisnis->category_sub;
        }else{
            $update['category_sub'] = old('category_sub');
        }
        if(old('description') == ''){
            $update['description'] = $bisnis->description;
        }else{
            $update['description'] = old('description');
        }
        // alamat
        if(old('provinsi') == ''){
            $update['provinsi'] = $bisnis->provinsi;
        }else{
            $update['provinsi'] = old('provinsi');
        }
        if(old('kabkota') == ''){
            $update['kabkota'] = $bisnis->kabkota;
        }else{
            $update['kabkota'] = old('kabkota');
        }
        if(old('kodepos') == ''){
            $update['kodepos'] = $bisnis->kodepos;
        }else{
            $update['kodepos'] = old('kodepos');
        }
        if(old('address') == ''){
            $update['address'] = $bisnis->address;
        }else{
            $update['address'] = old('address');
        }
        // kontak
        if(old('phone_number') == ''){
            $update['phone_number'] = $bisnis->phone_number;
        }else{
            $update['phone_number'] = old('phone_number');
        }
        if(old('fax_number') == ''){
            $update['fax_number'] = $bisnis->fax_number;
        }else{
            $update['fax_number'] = old('fax_number');
        }
        if(old('email') == ''){
            $update['email'] = $bisnis->email;
        }else{
            $update['email'] = old('email');
        }
        if(old('website') == ''){
            $update['website'] = $bisnis->website;
        }else{
            $update['website'] = old('website');
        }
        if(old('fb_link') == ''){
            $update['fb_link'] = $bisnis->fb_link;
        }else{
            $update['fb_link'] = old('fb_link');
        }
        if(old('twitter_link') == ''){
            $update['twitter_link'] = $bisnis->twitter_link;
        }else{
            $update['twitter_link'] = old('twitter_link');
        }
        if(old('instagram_link') == ''){
            $update['instagram_link'] = $bisnis->instagram_link;
        }else{
            $update['instagram_link'] = old('instagram_link');
        }
        // SEO
        if(old('seo_title') == ''){
            $update['seo_title'] = $bisnis->seo_title;
        }else{
            $update['seo_title'] = old('seo_title');
        }
        if(old('seo_keyword') == ''){
            $update['seo_keyword'] = $bisnis->seo_keyword;
        }else{
            $update['seo_keyword'] = old('seo_keyword');
        }
        if(old('seo_description') == ''){
            $update['seo_description'] = $bisnis->seo_description;
        }else{
            $update['seo_description'] = old('seo_description');
        }
        $arr_data['update'] = $update;

        // select
        $arr_data['categories'] = category::select('slug','name','count','id')
                                    ->whereNull('parent')->orderBy('name','asc')->get();
        $arr_data['categories_sub'] = category::select('slug','name','count','id')
                                    ->whereNotNull('parent')->where('parent',$update['category'])->orderBy('name','asc')->get();
        $arr_data['provinsies'] = region::select('slug','name','id')
                                    ->whereNull('parent')->orderBy('name','asc')->get();
        $arr_data['kabkotas']   = region::select('slug','name','id')
                                    ->whereNotNull('parent')->where('parent',$update['provinsi'])->orderBy('name','asc')->get();
        // end select

    	$arr_data['title']  = 'Bisnis '.$bisnis->name.' | '.$this->title;
        return view('user.bisnis.detail',$arr_data);
    }

    public function detailPost($id, Request $request){
        $bisnis = alamat::where('claim_by',\Auth::guard('user')->user()->id)
                        ->where('id',$id)->first();
        if(!$bisnis){
            Session::flash('notif-error', 'Data bisnis tidak ditemukan');
            return redirect(route('user.bisnis'));
        }

        $input = $request->all();
        $validator = Validator::make(
                        $input, [
                                'latitude' => 'required|numeric',
                                'longitude'=> 'required|numeric',
                                'name'     => 'required',
                                'category' => 'required',
                                'description' => 'required',
                                'provinsi' => 'required',
                                'kabkota'  => 'required',
                                'kodepos'  => 'required|numeric|digits:5',
                                'address'  => 'required',
                                'phone_number' => 'required|numeric',
                                'fax_number' => 'numeric',
                                'email'    => 'email',
                                'website'  => 'url',
                                'fb_link'  => 'url',
                                'twitter_link'  => 'url',
                                'instagram_link'=> 'url',
                                'file_logo'     => 'image|max:512'
                                ]
        );

        if ($validator->fails()) {
            return redirect(route('user.bisnis.detail',$bisnis->id))
                            ->withErrors($validator)->withInput();
        }
        // maps
        $bisnis->latitude  = $request->latitude;
        $bisnis->longitude = $request->longitude;
        // info
        $bisnis->name         = $request->input('name');
        $bisnis->category     = $request->input('category');
        $bisnis->category_sub = $request->input('category_sub');
        $bisnis->description  = $request->input('description');
        
        // alamat
        $bisnis->provinsi = $request->input('provinsi');
        $bisnis->kabkota  = $request->input('kabkota');
        $bisnis->kodepos  = $request->input('kodepos');
        $bisnis->address  = $request->input('address');
        // kontak
        $bisnis->phone_number   = $request->input('phone_number');
        $bisnis->fax_number     = $request->input('fax_number');
        $bisnis->email          = $request->input('email');
        $bisnis->website        = $request->input('website');
        $bisnis->fb_link        = $request->input('fb_link');
        $bisnis->twitter_link   = $request->input('twitter_link');
        $bisnis->instagram_link = $request->input('instagram_link');
        // SEO
        $bisnis->seo_title       = $request->input('seo_title');
        $bisnis->seo_keyword     = $request->input('seo_keyword');
        $bisnis->seo_description = $request->input('seo_description');

        if($request->file('file_logo') && $request->file('file_logo')->isValid()){
            $logo     = $request->file('file_logo');
            $base_url = 'uploads/'.date('Y/m/d/');

            $exist = file_exists($base_url);
            if(!$exist){
                mkdir($base_url, 0777, true);
            }

            $extension = $logo->getClientOriginalExtension();
            $fileNameOri  = 'logo-' . \Auth::guard('user')->user()->id . '.' . $extension;
            $fileNameSquare  = 'square-logo-' . \Auth::guard('user')->user()->id . '.' . $extension;
            $logo->move($base_url, $fileNameOri);
            
            Image::make($base_url . $fileNameOri)->widen(400)->save($base_url . $fileNameSquare);
            Image::make($base_url . $fileNameSquare)->resizeCanvas(400,400)->save();

            $bisnis->logo = $base_url . $fileNameOri;
            $bisnis->logo_filename = $fileNameOri;
        }
        $bisnis->save();
        
        Session::flash('notif-success', 'Update detail bisnis '.$bisnis->name.' berhasil.');
        return redirect(route('user.bisnis.detail',$bisnis->id));
    }
}
