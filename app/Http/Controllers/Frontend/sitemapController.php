<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use URL;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\category;
use App\Models\alamat;
use App\Models\pencarian;

class sitemapController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index(Request $request)
    {
    	$sitemap = \App::make("sitemap");

    	// if(env('APP_ENV') == 'production')
	    // 	$sitemap->setCache('laravel.sitemap-index', 3600);
	    
	    $sitemap->addSitemap(route('sitemap.bisnis'));
	    $sitemap->addSitemap(route('sitemap.static'));
        $sitemap->addSitemap(route('sitemap.pencarian'));
	    $sitemap->addSitemap(route('sitemap.categories'));

	    return $sitemap->render('sitemapindex');
    }

    public function staticPage(Request $request)
    {
    	$sitemap = \App::make("sitemap");
    	
   //  	if(env('APP_ENV') == 'production')
			// $sitemap->setCache('laravel.sitemap-static', 3600);

	    $sitemap->add(route('front'), date('Y-m-d H:i:s'), '1.0', 'daily');
	    
	    $sitemap->add(route('kategori.list'), '2016-10-31 23:59:59', '0.9', 'weekly');
	    $sitemap->add(route('area'), '2016-10-31 23:59:59', '0.9', 'weekly');
	    
	    $sitemap->add(route('static.term'), '2016-10-31 23:59:59', '0.8', 'monthly');
	    $sitemap->add(route('static.privacy'), '2016-10-31 23:59:59', '0.8', 'monthly');
	    $sitemap->add(route('static.disclaimer'), '2016-10-31 23:59:59', '0.8', 'monthly');
	    $sitemap->add(route('static.kontak'), '2016-10-31 23:59:59', '0.8', 'monthly');
	    
	    return $sitemap->render('xml');
    }

    public function categories(Request $request)
    {
    	$sitemap = \App::make("sitemap");
    	
   //  	if(env('APP_ENV') == 'production')
			// $sitemap->setCache('laravel.sitemap-categories', 3600);

		$categories = category::orderBy('updated_at', 'desc')->get();
	    foreach ($categories as $cat)
	    {
			$sitemap->add(route('kategori',$cat->slug), $cat->updated_at, '0.9', 'daily');
	    }
	    return $sitemap->render('xml');
    }

    public function pencarian(Request $request)
    {
    	$sitemap = \App::make("sitemap");
    	
   //  	if(env('APP_ENV') == 'production')
			// $sitemap->setCache('laravel.sitemap-pencarian', 3600);

		$pencarian = pencarian::orderBy('updated_at', 'desc')->get();
	    foreach ($pencarian as $cari)
	    {
            $key = explode('||', $cari->query_string);
			$sitemap->add(route('pencarian').'?lokasi='.$key[1].'&kategori='.$key[0].'&nama='.$key[2], $cari->updated_at, '0.9', 'daily');
        }
	    return $sitemap->render('xml');
    }

    public function detailAlamat(Request $request)
    {
    	$sitemap = \App::make("sitemap");

        // if(env('APP_ENV') == 'production')
        //     $sitemap->setCache('laravel.sitemap-alamat', 3600);

        $products = alamat::orderBy('updated_at', 'desc')->paginate(5000);
        foreach ($products as $prod)
        {
            $sitemap->add(route('lokasi', $prod->slug), $prod->updated_at, '0.9', 'daily');
        }
        return $sitemap->render('xml');
    }
}
