<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use Session;

use App\Models\kontak;

class staticPageController extends Controller
{
    public function __construct()
    {
    }

    public function pageFaq(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();

    	$arr_data['title'] = 'FAQ | '.$this->title;
        return view('frontend.page.faq',$arr_data);
    }

    public function pageTerm(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();

    	$arr_data['title'] = 'Syarat dan Ketentuan | '.$this->title;
        return view('frontend.page.term',$arr_data);
    }

    public function pagePrivacy(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();

    	$arr_data['title'] = 'Syarat dan Ketentuan | '.$this->title;
        return view('frontend.page.privacy',$arr_data);
    }

    public function pageSitemap(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();

        $arr_data['title'] = 'Sitemap | '.$this->title;
        return view('frontend.page.sitemap',$arr_data);
    }

    public function pageDisclaimer(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();

    	$arr_data['title'] = 'Sangkalan | '.$this->title;
        return view('frontend.page.disclaimer',$arr_data);
    }

    public function pageKontak(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();

    	$arr_data['title'] = 'Kontak | '.$this->title;
        return view('frontend.page.kontak',$arr_data);
    }

    public function pageKontakPost(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();

    	$input = $request->all();
        $validator = Validator::make(
                        $input, [
                                'nama'  => 'required',
                                'email' => 'required|email',
                                'pesan' => 'required',
                                'no_hp' => 'numeric',
                                ]
        );

        if ($validator->fails()) {
            return redirect(route('static.kontak'))
                            ->withErrors($validator)->withInput();
        }

        $data        = new kontak();
        $data->nama  = $request->input('nama');
        $data->email = $request->input('email');
        $data->pesan = $request->input('pesan');
        $data->no_hp = $request->input('no_hp');
        $data->save();

        Session::flash('notif-success', 'pesan berhasil terkirim');
        return redirect(route('static.kontak'));
    }
}
