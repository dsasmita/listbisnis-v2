<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Session;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\category;
use App\Models\region;
use App\Models\alamat;
use App\Models\commentRating;
use App\Models\pencarian;

class frontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.png');

        $arr_data['categories'] = category::whereNull('parent')->orderby('name','asc')->get();
        $arr_data['title']      = $this->title.' | Direktori Bisnis Indonesia dan Temukan Peluang Penawaran Kerjasama Bisnis dan Usaha di Indonesia';
        return view('frontend.front.welcome',$arr_data);
    }

    public function kategori($category, Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        
        $category   = category::select('slug','name','parent')
                                                ->where('slug', $category)->first();
        $arr_data['category'] = $category;
        if(!$arr_data['category']){
            Session::flash('notif-error', 'Category tidak ditemukan');
            return redirect(route('front'));
        }

        $category->views = $category->views + 1;
        $category->save();

        $arr_data['statusChild'] = false;
        if(is_null($arr_data['category']->parent)){
            $arr_data['statusChild'] = true;
            $arr_data['categoryChild'] = category::select('slug','name')->where('parent',$arr_data['category']->slug)
                                            ->orderBy('name','asc')
                                            ->get();
        }else{
            $arr_data['categoryChild'] = category::select('slug','name')->where('parent',$arr_data['category']->parent)
                                            ->orderBy('name','asc')
                                            ->get();
        }

        $arr_data['list'] = alamat::with(['kategori','subKategori'])->where(function($query) use($arr_data){
                                        if($arr_data['statusChild']){
                                            $query->where('category',$arr_data['category']->slug);
                                        }else{
                                            $query->where('category_sub',$arr_data['category']->slug);
                                        }
                                })->orderBy('created_at','desc')->paginate($this->limit);
        
        // SEO
        $arr_data['seo']['title']       = $arr_data['category']->name.' | '.$this->title;
        $arr_data['seo']['keywords']    = $arr_data['category']->name.', '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description'] = 'List Bisnis yang berfokus pada bidang '.$arr_data['category']->name.', temukan peluang dan lokasi bisnis dengan kategori '.$arr_data['category']->name;

        $arr_data['title'] = $arr_data['category']->name.' | '.$this->title;
        return view('frontend.front.category',$arr_data);
    }

    public function kategoriList(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();
        
        // SEO
        $arr_data['seo']['title']       = 'Daftar Seluruh Kateogri Bisnis | '.$this->title;
        $arr_data['seo']['keywords']    = 'kategori bisnis, '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description'] = 'Daftar seluruh kategori yang terdaftar pada Lis Bisnis Indonesia';

        $arr_data['title'] = 'Daftar Seluruh Kateogri Bisnis | '.$this->title;
        return view('frontend.front.category-list',$arr_data);
    }

    public function wilayah(Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();
        
        // SEO
        $arr_data['seo']['title']       = 'Daftar Lokasi | '.$this->title;
        $arr_data['seo']['keywords']    = 'wilayah, '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description'] = 'Daftar seluruh wilayah yang terdaftar pada Lis Bisnis Indonesia';

        $arr_data['title'] = 'Daftar Lokasi | '.$this->title;
        return view('frontend.front.area',$arr_data);
    }

    public function wilayahDetail($reg, Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['url'] = $request->url();
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        
        $region = region::select('slug','name','parent')        
                    ->where('slug', $reg)->first();
        
        $arr_data['region'] = $region;
        if(!$arr_data['region']){
            Session::flash('notif-error', 'wilayah tidak ditemukan');
            return redirect(route('front'));
        }

        $region->views = $region->views + 1;
        $region->save();

        $arr_data['statusChild'] = false;
        if(is_null($arr_data['region']->parent)){
            $arr_data['statusChild'] = true;
            $arr_data['regionChild'] = region::select('slug','name')->where('parent',$arr_data['region']->slug)
                                            ->orderBy('name','asc')
                                            ->get();
        }else{
            $arr_data['regionChild'] = region::select('slug','name')->where('parent',$arr_data['region']->parent)
                                            ->orderBy('name','asc')
                                            ->get();
        }

        $arr_data['list'] = alamat::with(['kategori','subKategori'])->where(function($query) use($arr_data){
                                        if($arr_data['statusChild']){
                                            $query->where('provinsi',$arr_data['region']->slug);
                                        }else{
                                            $query->where('kabkota',$arr_data['region']->slug);
                                        }
                                })->orderBy('created_at','desc')->paginate($this->limit);
        // dd($arr_data['regionChild']);
        // SEO
        $arr_data['seo']['title']       = $arr_data['region']->name.' | '.$this->title;
        $arr_data['seo']['keywords']    = $arr_data['region']->name.', '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description'] = 'List Bisnis yang berfokus pada bidang '.$arr_data['region']->name.', temukan peluang dan lokasi bisnis dengan kategori '.$arr_data['region']->name;

        $arr_data['title'] = $arr_data['region']->name.' | '.$this->title;
        return view('frontend.front.region',$arr_data);
    }

    public function pencarian(Request $request)
    {
        $arr_data              = [];
        $arr_data['seo']       = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();
        
        $arr_data['input']     = $request->all();
        $arr_data['pencarian'] = $request->all();
        $arr_data['list']      = alamat::with(['kategori','subKategori'])->where(function($query) use($arr_data){
                                    if(@$arr_data['input']['nama'] && @$arr_data['input']['nama'] != ''){
                                        $query->where('name','like','%'.@$arr_data['input']['nama'].'%');
                                    }
                                    
                                    if(@$arr_data['input']['lokasi'] && @$arr_data['input']['lokasi'] != ''){
                                        $query->where('address','like','%'.$arr_data['input']['lokasi'].'%');
                                    }
                                })->where(function($query) use($arr_data){
                                    if(@$arr_data['input']['kategori'] && @$arr_data['input']['kategori'] != ''){
                                        $query->where('category',$arr_data['input']['kategori']);
                                        $query->orWhere('category_sub',$arr_data['input']['kategori']);
                                    }
                                })->orderBy('created_at','desc')->paginate($this->limit);

        $title = 'Hasil semua pencarian';
        if(@$request->input('lokasi') != '' || @$request->input('kategori') != '' || @$request->input('nama') != ''){
            $title = 'Hasil pencarian berdasarkan ';
            if($request->input('lokasi') != ''){
                $title = $title . ' lokasi '.$request->input('lokasi');
            }
            if($request->input('kategori') != ''){
                $title = $title . ' kategori '.$request->input('kategori');
            }
            if($request->input('nama') != ''){
                $title = $title . ' nama '.$request->input('nama');
            }
        }

        // search update table
        $search = [
                    'lokasi' => $request->input('lokasi'),
                    'kategori' => $request->input('kategori'),
                    'nama' => $request->input('nama'),
                    ];
        ksort($search);
        $stringSearch = strtolower(implode('||',$search));
        $cari = pencarian::where('query_string',$stringSearch)->first();
        if(!$cari){
            $cari = new pencarian();
            $cari->query_string = $stringSearch;
            // if(trim($request->lokasi) != '')
            //     $cari->lokasi = trim($request->lokasi);
            // if(trim($request->kategori) != '')
            //     $cari->kategori = trim($request->kategori);
            // if(trim($request->nama) != '')
            //     $cari->nama = trim($request->nama);
            $cari->count = 1;
            $cari->save();
        }else{
            $cari->count = 1 + $cari->count;
            $cari->save();
        }
        // end search update
        $arr_data['search'] = $request->input();

        $arr_data['seo']['title']       = $title.' | '.$this->title;
        $arr_data['seo']['keywords']    = $title.', '.$arr_data['seo']['keywords'];
        $arr_data['seo']['description'] = $title;

        $arr_data['title'] = $title.' | '.$this->title;
        $arr_data['title_html'] = $title;
        return view('frontend.front.pencarian',$arr_data);
    }

    public function lokasi($slug, Request $request)
    {
        $arr_data = [];
        $arr_data['seo'] = $this->seoPack;
        $arr_data['seo']['image'] = asset('assets/images/banner.png');
        $arr_data['seo']['url'] = $request->url();
        
        $arr_data['lokasi']   = alamat::with(['kategori','subKategori','pesan'])->where('slug', $slug)->first();
        if(!$arr_data['lokasi']){
            Session::flash('notif-error', 'lokasi tidak ditemukan');
            return redirect(route('front'));
        }
        $arr_data['kategori'] = [];
        if($arr_data['lokasi']->kategori()->first()){
            $arr_data['kategori'] = $arr_data['lokasi']->kategori()->first();
        }
        $arr_data['subKategori'] = [];
        if($arr_data['lokasi']->subKategori()->first()){
            $arr_data['subKategori'] = $arr_data['lokasi']->subKategori()->first();
        }
        $arr_data['pesan'] = [];
        if($arr_data['lokasi']->pesan()->first()){
            $arr_data['pesan'] = $arr_data['lokasi']->pesan()->get();
        }

        $arr_data['star']['full'] = floor($arr_data['lokasi']->star / 1);
        $arr_data['star']['half'] = ceil($arr_data['lokasi']->star / 1) - floor($arr_data['lokasi']->star / 1);
        $arr_data['star']['zero'] = 5 - $arr_data['star']['full'] - $arr_data['star']['half'];

        $arr_data['lokasi']->views = $arr_data['lokasi']->views + 1;
        $arr_data['lokasi']->save();

        // SEO
        if($arr_data['lokasi']->seo_title != ''){
            $arr_data['seo']['title'] = $arr_data['lokasi']->seo_title;
        }else{
            $arr_data['seo']['title'] = $arr_data['lokasi']->name.' | '.$this->title;
        }
        if($arr_data['lokasi']->seo_keyword != ''){
            $arr_data['seo']['keywords'] = $arr_data['lokasi']->seo_keyword;
        }else{
            $arr_data['seo']['keywords']    = $arr_data['lokasi']->name.', '.$arr_data['seo']['keywords'];
        }
        if($arr_data['lokasi']->seo_description != ''){
            $arr_data['seo']['description'] = $arr_data['lokasi']->seo_description;
        }else{
            if($arr_data['lokasi']->description != ''){
                $arr_data['seo']['description'] = $arr_data['lokasi']->description;
            }else{
                $arr_data['seo']['description'] = "Bisnis ".$arr_data['lokasi']->name." adalah bisnis yang bergerak di bidang ".$arr_data['kategori']->name.
                                    " yang khusus menangani bisnis ".$arr_data['subKategori']->name;
            }
        }
        // end SEO

        $arr_data['title'] = $arr_data['lokasi']->name.' | '.$this->title;
        return view('frontend.front.detail-alamat',$arr_data);
    }

    public function lokasiPesan($slug, Request $request)
    {
        $lokasi = alamat::where('slug', $slug)->first();
        if(!$lokasi){
            Session::flash('notif-error', 'lokasi tidak ditemukan');
            return redirect(route('front'));
        }
        $input = $request->all();
        $validator = Validator::make(
                        $input, [
                                'nama'  => 'required',
                                'email' => 'required|email',
                                'pesan' => 'required',
                                'star'  => 'numeric'
                                ]
        );

        if ($validator->fails()) {
            return redirect(route('lokasi',$lokasi->slug))
                            ->withErrors($validator)->withInput();
        }

        $pesan = new commentRating();
        $pesan->id_lokasi = $lokasi->id;
        $pesan->nama      = $request->input('nama'); 
        $pesan->email     = $request->input('email');
        $pesan->no_hp     = $request->input('no_hp');
        $pesan->pesan     = $request->input('pesan'); 
        $lokasi->pesan_counter = $lokasi->pesan_counter + 1;
        
        if($request->input('star') != '' && is_numeric($request->input('star'))){
            $pesan->star  = $request->input('star');

            $lokasi->star = (($lokasi->star*$lokasi->star_counter) + $request->input('star'))/($lokasi->star_counter+1);
            $lokasi->star_counter = $lokasi->star_counter + 1;
        }
        $lokasi->save();
        $pesan->save();

        Session::flash('notif-success', 'pesan berhasil terkirim');
        return redirect(route('lokasi',$lokasi->slug));
    }

    public function claimBisnis($id, Request $request)
    {
        if (\Auth::guard('user')->check()) {
            $alamat = alamat::whereNull('claim_by')->where('id',$id)->first();
            if($alamat){
                $alamat->claim_by = \Auth::guard('user')->user()->id;
                $alamat->save();

                Session::flash('notif-success', 'Silahkan update detail bisnis Anda.');
                return redirect(route('user.bisnis.detail',$alamat->id));
            }else{
                Session::flash('notif-error', 'Bisnis tidak ditemukan atau telah dikelola pihak lain.');
                return redirect(route('user.bisnis'));
            }
        }else{
            Session::flash('notif-warning', 'Silahkan login untuk melakukan klaim bisnis.');
            return redirect(route('user.login'));
        }
    }
}
