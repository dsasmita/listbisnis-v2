<?php

namespace App\Http\Controllers\Additional;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\category;
use App\Models\region;

class additionalController extends Controller
{
	public function categoriesJson(Request $request){

	}

	public function categoriesSubJson(Request $request){
		$parent = category::select('slug')
					->where('slug', $request->input('slug'))
					->whereNull('parent')->first();
		
		if(!$parent){
			return response()->json([
								'status'  => 'NOT-OK',
								'message' => 'category not found'
							]);
		}

		$category = category::select('slug','name')->orderBy('name','asc')
								->where('parent', $parent->slug)->get();

		return response()->json([
								'status'  => 'OK',
								'count'   => $category->count(),
								'list'    => $category,
								'message' => ''
							]);
	}

    public function provinsiJson(Request $request){

    }

    public function kabkotaJson(Request $request){
    	$parent = region::select('slug')
					->where('slug', $request->input('slug'))
					->whereNull('parent')->first();
		
		if(!$parent){
			return response()->json([
								'status'  => 'NOT-OK',
								'message' => 'region not found'
							]);
		}

		$region = region::select('slug','name')->orderBy('name','asc')
								->where('parent', $parent->slug)->get();

		return response()->json([
								'status'  => 'OK',
								'count'   => $region->count(),
								'list'    => $region,
								'message' => ''
							]);
    }
}
