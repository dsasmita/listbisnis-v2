<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;

use App\Models\category;
use App\Models\region;
use App\Models\alamat;
use App\Models\commentRating;
use App\Models\pencarian;

class alamatController extends ApiGuardController
{
    public function index(Request $request)
    {
    	$method = 'api.alamat.list';

    	$limit  = $request->input('limit',15);
        $select = explode(',', $request->input('select','*'));
        $order  = $request->input('order','created_at');
    	$sort   = $request->input('sort','desc');

        $data   = alamat::select($select)
                          ->orderBy($order,$sort)
                          ->paginate($limit);
    	
        return response()->json([
                            'method'    => $method,
                            'status'    => 'OK',
                            'message'   => 'well done dude!',
                            'code'      => "AUTHORIZED",
                            'data'		=> $data,	
                            'http_code' => 201
                          ],201);
    }
}
