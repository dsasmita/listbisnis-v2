<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;

use App\Models\category;

class categoryController extends ApiGuardController
{
    public function index(Request $request)
    {
    	$method = 'api.category.list';

    	$limit  = $request->input('limit',15);
        $select = explode(',', $request->input('select','*'));
        $order  = $request->input('order','created_at');
    	$sort   = $request->input('sort','desc');

        $data   = category::select($select)
                          ->orderBy($order,$sort)
                          ->paginate($limit);
    	
        return response()->json([
                            'method'    => $method,
                            'status'    => 'OK',
                            'message'   => 'well done dude!',
                            'code'      => "AUTHORIZED",
                            'data'		=> $data,	
                            'http_code' => 201
                          ],201);
    }
}
