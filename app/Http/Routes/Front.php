<?php

//=============//
//             //
// Route Front //
//             //
//=============//

$appRoutes = function() {
    Route::group(['middleware' => ['web'],'namespace' => 'Frontend'], function () {
        Route::get('/', 
            ['as' => 'front', 
             'uses' => 'frontController@index']
        );
        Route::get('kategori/{id}', 
            ['as' => 'kategori', 
             'uses' => 'frontController@kategori']
        );
        Route::get('kategori', 
            ['as' => 'kategori.list', 
             'uses' => 'frontController@kategoriList']
        );
        Route::get('wilayah', 
            ['as' => 'area', 
             'uses' => 'frontController@wilayah']
        );
        Route::get('wilayah/{id}', 
            ['as' => 'area.detail', 
             'uses' => 'frontController@wilayahDetail']
        );
        Route::get('pencarian', 
            ['as' => 'pencarian', 
             'uses' => 'frontController@pencarian']
        );
        Route::get('bisnis/{id}', 
            ['as' => 'lokasi', 
             'uses' => 'frontController@lokasi']
        );
        Route::post('bisnis/{id}', 
            ['as' => 'lokasi.post', 
             'uses' => 'frontController@lokasiPesan']
        );
        Route::get('klaim-bisnis-{id}', 
            ['as' => 'claim.bisnis', 
             'uses' => 'frontController@claimBisnis']
        );

        //=================//
        //-----static------//
        //=================//

        Route::get('page/faq', 
            ['as' => 'static.faq', 
             'uses' => 'staticPageController@pageFaq']
        );
        Route::get('page/syarat-ketentuan', 
            ['as' => 'static.term', 
             'uses' => 'staticPageController@pageTerm']
        );
        Route::get('page/kebijakan-privasi', 
            ['as' => 'static.privacy', 
             'uses' => 'staticPageController@pagePrivacy']
        );
        Route::get('page/sangkalan', 
            ['as' => 'static.disclaimer', 
             'uses' => 'staticPageController@pageDisclaimer']
        );
        Route::get('sitemap', 
            ['as' => 'static.sitemap', 
             'uses' => 'staticPageController@pageSitemap']
        );
        Route::get('page/kontak', 
            ['as' => 'static.kontak', 
             'uses' => 'staticPageController@pageKontak']
        );
        Route::post('page/kontak', 
            ['as' => 'static.kontak.post', 
             'uses' => 'staticPageController@pageKontakPost']
        );


        //=================//
        //-----sitemap-----//
        //=================//

        Route::get('sitemap.xml', 
            ['as' => 'sitemap.home', 
             'uses' => 'sitemapController@index']
        );
        Route::get('sitemap-static.xml', 
            ['as' => 'sitemap.static', 
             'uses' => 'sitemapController@staticPage']
        );
        Route::get('sitemap-pencarian.xml', 
            ['as' => 'sitemap.pencarian', 
             'uses' => 'sitemapController@pencarian']
        );
        Route::get('sitemap-categories.xml', 
            ['as' => 'sitemap.categories', 
             'uses' => 'sitemapController@categories']
        );
        Route::get('sitemap-bisnis.xml', 
            ['as' => 'sitemap.bisnis', 
             'uses' => 'sitemapController@detailAlamat']
        ); 
    });
};

//=================//
//---additional----//
//=================//
$appRoutesAdd = function() {
    Route::group(['middleware' => ['web'], 'prefix' => 'data', 'namespace' => 'Additional'], function(){

        Route::get('categories-sub.json', 
            ['as' => 'additional.categoriessub.json', 
             'uses' => 'additionalController@categoriesSubJson']
        );
        Route::get('kabkota.json', 
            ['as' => 'additional.kabkota.json', 
             'uses' => 'additionalController@kabkotaJson']
        );
    });
};

$domain = explode('|', env('URL_FRONT'));
foreach ($domain as $key => $value) {
    Route::group(array('domain' => $value), $appRoutes);
    Route::group(array('domain' => $value), $appRoutesAdd);
}