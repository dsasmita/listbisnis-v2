<?php

//=============//
//             //
// Route Users //
//             //
//=============//

Route::group(['middleware' => ['web'], 'prefix' => env('URL_USER_PATH'), 'domain' => env('URL_FRONT')], function(){
    //Auth route
    Route::get('login', ['as' => 'user.login', 'uses' => 'Auth\AuthController@showLoginForm']);
    Route::post('login', ['as' => 'user.login.post', 'uses' => 'Auth\AuthController@loginPost']);
    Route::get('logout', ['as' => 'user.logout', 'uses' => 'Auth\AuthController@logout']);

    // Registration Routes...
    Route::get('register', ['as' => 'user.register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
    Route::post('register', ['as' => 'user.register.post', 'uses' => 'Auth\AuthController@register']);

    // Registration social media
    Route::get('facebook-login', ['as' => 'user.facebook.redirect', 'uses' => 'Auth\SocialAuthController@facebookRedirect']);
    Route::get('facebook-callback', ['as' => 'user.facebook.callback', 'uses' => 'Auth\SocialAuthController@facebookCallback']);
    
    Route::get('twitter-login', ['as' => 'user.twitter.redirect', 'uses' => 'Auth\SocialAuthController@twitterRedirect']);
    Route::get('twitter-callback', ['as' => 'user.twitter.callback', 'uses' => 'Auth\SocialAuthController@twitterCallback']);

    Route::get('google-login', ['as' => 'user.google.redirect', 'uses' => 'Auth\SocialAuthController@googleRedirect']);
    Route::get('google-callback', ['as' => 'user.google.callback', 'uses' => 'Auth\SocialAuthController@googleCallback']);


    // Password Reset Routes...
    Route::get('forget-password', ['as' => 'user.password.email', 'uses' => 'Auth\PasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'user.password.email.post', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token?}', ['as' => 'user.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'user.password.reset.post', 'uses' => 'Auth\PasswordController@reset']);

    Route::group(['middleware' => ['auth'],'namespace' => 'User'], function(){
        Route::get('/', ['as' => 'user.home', 'uses' => 'homeController@index']);   
        
        Route::get('/bisnis', ['as' => 'user.bisnis', 'uses' => 'bisnisController@index']);   
        Route::get('/bisnis/tambah', ['as' => 'user.bisnis.create', 'uses' => 'bisnisController@create']);   
        Route::post('/bisnis/tambah', ['as' => 'user.bisnis.create', 'uses' => 'bisnisController@createPost']);   
        Route::get('/bisnis/{id}', ['as' => 'user.bisnis.detail', 'uses' => 'bisnisController@detail']);   
        Route::post('/bisnis/{id}', ['as' => 'user.bisnis.detail.post', 'uses' => 'bisnisController@detailPost']);

        Route::get('/profile', ['as' => 'user.profile', 'uses' => 'profileController@index']);   
        Route::post('/profile/update', ['as' => 'user.profile.update', 'uses' => 'profileController@updateProfile']);
        Route::post('/profile/password', ['as' => 'user.profile.password', 'uses' => 'profileController@updatePassword']);
    });
});