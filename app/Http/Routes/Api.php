<?php

//=============//
//             //
// Route API   //
//             //
//=============//

// b846e7108252aa06208d41ef65382dc40ebfaf30 prod
// versi awal, cuman buat end user ae jadi api
// - list bisnis
// - kategori
// - province
// - search
// - detail organisasi / PT / UKM
// - penawaran
// - syncronisasi sistemmu dg google firebase untuk manage usernya
// - kenapa user di mirror di database, 
//   krn android sudah settle konek dg google firebase, ada mekanisme session juga

Route::group([
			'middleware' => ['web'], 
			'prefix' => env('URL_API_PATH'), 
			'domain' => env('URL_API'),
			'namespace' => 'Api'],function(){

		Route::post('alamat/list', 
	        ['as' => 'api.alamat.list', 
	         'uses' => 'alamatController@index']
	    );

	    Route::post('category/list', 
	        ['as' => 'api.category.list', 
	         'uses' => 'categoryController@index']
	    );
});