<?php

//===============//
//               //
// Route Backend //
//               //
//===============//

Route::group(['middleware' => ['web'], 'domain' => env('URL_ADMIN')], function () {
    //Login Routes...
    Route::get('/login',['as' => 'admin.login', 'uses' => 'AdminAuth\AuthController@showLoginForm']);
    Route::post('/login',['as' => 'admin.login.post', 'uses' => 'AdminAuth\AuthController@login']);
    Route::get('/logout',['as' => 'admin.logout', 'uses' => 'AdminAuth\AuthController@logout']);

    Route::get('/scrapt-alamatku',['as' => 'admin.scrapt.alamatku', 'uses' => 'Admin\scraperController@alamtTerbaru']);
    
    Route::get('/normalisasi-koordinat',['as' => 'admin.scrapt.alamatku', 'uses' => 'Admin\scraperController@normalisasiKoordinat']);

    Route::group(['middleware' => ['admin'],'namespace' => 'Admin'], function(){
        Route::get('/', ['as' => 'admin.home', 'uses' => 'homeController@index']);   
        Route::get('/home', ['as' => 'admin.home2', 'uses' => 'homeController@index']);   
        
        Route::get('/categories', ['as' => 'admin.categories', 'uses' => 'categoriesController@index']);   
        Route::get('/pencarian', ['as' => 'admin.pencarian', 'uses' => 'pencarianController@index']);   
        Route::get('/bisnis', ['as' => 'admin.bisnis', 'uses' => 'bisnisController@index']);   
        
        Route::get('/migrate-to-mysql/region', ['as' => 'admin.migrate.region', 'uses' => 'migrateToMysqlController@region']);   
        Route::get('/migrate-to-mysql/category', ['as' => 'admin.migrate.region', 'uses' => 'migrateToMysqlController@category']);   
        Route::get('/migrate-to-mysql/alamat', ['as' => 'admin.migrate.alamat', 'uses' => 'migrateToMysqlController@alamat']);   
        
        Route::get('/normalisasi/kategori', ['as' => 'admin.normalisasi.kategori', 'uses' => 'scraperController@kategoriNormalisasi']);   
    });
});