<?php

/**
 * Routes for Referral tunaiku core
 *
 * @author      Dadang Sasmita <dangs.work@gmail.com>
 *
 * @version     1.0
 */

$routes = new FilesystemIterator(__DIR__ . '/Routes');

foreach ($routes as $file):
    if ($file->isFile() AND strtolower(pathinfo($file->getFilename(), PATHINFO_EXTENSION)) == 'php') include_once $file->getPathName();
endforeach;
