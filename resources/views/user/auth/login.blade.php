@extends('user.layouts.app')

@section('content')
<form class="login-form" action="{{ route('user.login.post') }}" method="post">
    {{ csrf_field() }}
    <h3 class="form-title">Login to your account</h3>
    <div class="hidden alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span> Enter any username and password. </span>
    </div>
    <!-- @if($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span> {{ $error }} </span>
            </div>
        @endforeach
    @endif -->
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="login" value="{{ old('login') }}" /> </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
    </div>
    <div class="form-actions">
        <label class="checkbox">
            <input type="checkbox" name="remember" value="1" /> Remember me </label>
        <button type="submit" class="btn green pull-right"> Login </button>
    </div>
    <div class="login-options">
        <h4>Or login with</h4>
        <ul class="social-icons">
            <li>
                <a class="facebook" data-original-title="facebook" href="{{ route('user.facebook.redirect') }}"> </a>
            </li>
            <li>
                <a class="twitter" data-original-title="Twitter" href="{{ route('user.twitter.redirect') }}"> </a>
            </li>
            <li>
                <a class="googleplus" data-original-title="Goole Plus" href="{{ route('user.google.redirect') }}"> </a>
            </li>
        </ul>
    </div>
    <div class="forget-password">
        <h4>Forgot your password ?</h4>
        <p> no worries, click
            <a href="{{ route('user.password.email') }}"> here </a> to reset your password. </p>
    </div>
    <div class="create-account">
        <p> Don't have an account yet ?&nbsp;
            <a href="{{ route('user.register') }}" class="font-red-thunderbird"> Create an account </a>
        </p>
    </div>
</form>
@endsection
