@extends('user.layouts.app')

@section('title') List Bisnis Indonesia - Reset Password @endsection

@section('content')
<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="forget-form" action="{{ route('user.password.reset.post') }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="token" value="{{ $token }}">
    <h3>Reset Password ?</h3>
    <p> Enter your e-mail and new password below to reset your password. </p>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span> {{ $error }} </span>
            </div>
        @endforeach
    @endif
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" value="{{ old('email') }}" placeholder="Email" name="email" /> </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" value="" placeholder="Password" name="password" /> </div>
    </div>
    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" value="" placeholder="Password Confirmation" name="password_confirmation" /> </div>
    </div>
    <div class="form-actions form-actions-buttom">
        <button type="submit" class="btn green pull-right btn-block"> Submit </button>
    </div>
    <div class="">
        <p> Already have account? <a href="{{ route('user.login') }}" class="font-red-thunderbird"> here </a> to login</p>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->
@endsection
