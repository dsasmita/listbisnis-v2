@extends('user.layouts.app')

@section('title') List Bisnis Indonesia - Register @endsection

@section('content')
<form class="register-form" action="{{ route('user.register.post') }}" method="post">
    {{ csrf_field() }}
    <h3>Sign Up</h3>
    <!-- @if($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span> {{ $error }} </span>
            </div>
        @endforeach
    @endif -->
    <p> Enter your personal details below: </p>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Full Name</label>
        <div class="input-icon">
            <i class="fa fa-font"></i>
            <input class="form-control placeholder-no-fix" type="text" placeholder="Full Name" name="name" value="{{ old('name') }}"/> </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" value="{{ old('email') }}"/> </div>
    </div>
    <p> Enter your account details below: </p>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" value="{{ old('username') }}" /> </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" /> </div>
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
        <div class="controls">
            <div class="input-icon">
                <i class="fa fa-check"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation" /> </div>
        </div>
    </div>
    <div class="form-group">
        <label>
            <input type="checkbox" name="tos" value="1" /> I agree to the
            <a href="javascript:;"> Terms of Service </a> and
            <a href="javascript:;"> Privacy Policy </a>
        </label>
        <div id="register_tnc_error"> </div>
    </div>
    <div class="form-actions form-actions-buttom">
        <button type="submit" id="register-submit-btn" class="btn green pull-right btn-block"> Sign Up </button>
    </div>

    <div class="create-account">
        <p> Already have account?
            <a href="{{ route('user.login') }}" class="font-red-thunderbird"> Login </a>
        </p>
    </div>
</form>
@endsection
