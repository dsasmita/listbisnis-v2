@extends('user.layouts.default')

@section('css')
<link href="{{asset('assets-app/assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content" style="background-color: #FAFAFA!important;">
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<a href="{{ route('front') }}">Beranda</a>
				<i class="fa fa-circle"></i>
			</li>
			<li>
				<span>Profil</span>
			</li>
		</ul>
	</div>
	<h3 class="page-title"> Profil
		<small>{{\Auth::guard('user')->user()->name}}</small>
	</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="profile-sidebar">
				<div class="portlet light profile-sidebar-portlet ">
					<div class="profile-userpic">
						<img src="{{url('assets-app/assets/layouts/layout/img/avatar.jpg')}}" class="img-responsive" alt=""> </div>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name"> {{$profil->name}} </div>
					</div>
					<div class="profile-usermenu">
						<ul class="nav">
							<li class="active">
								<a href="#">
									<i class="icon-wrench"></i> Setting </a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="profile-content">
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light ">
							<div class="portlet-title tabbable-line">
								<div class="caption caption-md">
									<i class="icon-globe theme-font hide"></i>
									<span class="caption-subject font-blue-madison bold uppercase">Ubah Profil</span>
								</div>
								<ul class="nav nav-tabs">
									<li class="{{ Session::get('profile-action') == 'profile' || Session::get('profile-action') == '' ? 'active' : '' }}">
										<a href="#tab_1_1" id="nav-profile-info" data-toggle="tab">Info</a>
									</li>
									<li class="hidden">
										<a href="#tab_1_2" data-toggle="tab">Avatar</a>
									</li>
									<li class="{{ (Session::get('profile-action') == 'password')? 'active' : '' }}">
										<a href="#tab_1_3" id="nav-profile-password" data-toggle="tab">Password</a>
									</li>
								</ul>
							</div>
							<div class="portlet-body">
								<div class="tab-content">
									<div class="tab-pane {{ (Session::get('profile-action') == 'profile' || Session::get('profile-action') == '')? 'active' : '' }}" id="tab_1_1">
										<form role="form" method="post" action="{{ route('user.profile.update') }}">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="form-group">
												<label class="control-label">Nama</label>
												<input type="text" value="{{$profil->name}}" name="name" placeholder="nama anda" class="form-control" />
											</div>
											<div class="form-group">
												<label class="control-label">Email</label>
												<input type="text" disabled="" value="{{$profil->email}}" name="email" placeholder="email anda" class="form-control" /> 
											</div>
											<div class="form-group">
												<label class="control-label">Gender</label>
												<select class="form-control" name="gender">
													<option value="">-gender-</option>
													<option {{$profil->gender == 'male' ? 'selected':'' }} value="male">Laki - laki</option>
													<option {{$profil->gender == 'female' ? 'selected':'' }} value="female">Perempuan</option>
												</select>
											</div>
											<div class="margiv-top-10">
												<button type="submit" class="btn green">Simpan Profil</button>
											</div>
										</form>
									</div>
								
									<div class="tab-pane" id="tab_1_2">
										<p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
											eiusmod. </p>
										<form action="#" role="form">
											<div class="form-group">
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
														<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
													<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
													<div>
														<span class="btn default btn-file">
															<span class="fileinput-new"> Select image </span>
															<span class="fileinput-exists"> Change </span>
															<input type="file" name="..."> </span>
														<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>
												<div class="clearfix margin-top-10">
													<span class="label label-danger">NOTE! </span>
													<span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
												</div>
											</div>
											<div class="margin-top-10">
												<a href="javascript:;" class="btn green"> Submit </a>
												<a href="javascript:;" class="btn default"> Cancel </a>
											</div>
										</form>
									</div>
								
									<div class="tab-pane {{ (Session::get('profile-action') == 'password')? 'active' : '' }}" id="tab_1_3">
										<form method="post" action="{{ route('user.profile.password') }}">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											@if($profil->password != '')
												<div class="form-group">
													<label class="control-label">Current Password</label>
													<input type="password" name="password_old" class="form-control" /> </div>
											@endif
											<div class="form-group">
												<label class="control-label">New Password</label>
												<input type="password" name="password" class="form-control" /> </div>
											<div class="form-group">
												<label class="control-label">Re-type New Password</label>
												<input type="password" name="password_confirmation" class="form-control" /> </div>
											<div class="margin-top-10">
												<button type="submit" class="btn green">Ubah Password</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$( document ).ready(function() {
	@if(Session::get('profile-action') == 'profile')
		$('#nav-profile-info').trigger('click');
	@endif
	@if(Session::get('profile-action') == 'password')
		$('#nav-profile-password').trigger('click');
	@endif
});
</script>
@endsection