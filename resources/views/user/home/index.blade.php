@extends('user.layouts.default')

@section('css')
@endsection

@section('content')
<div class="page-content">
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('front') }}">Beranda</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Dashboard
        <small>dashboard & statistik</small>
    </h3>
    <div class="row">
        <div class="col-md-12">
            <p><em><center>Selamat Datang <strong>{{ \Auth::guard('user')->user()->name }}</strong>!</center></em></p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection

@section('javascript')
@endsection