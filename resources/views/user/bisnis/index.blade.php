@extends('user.layouts.default')

@section('css')
@endsection

@section('content')
<div class="page-content">
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('front') }}">Beranda</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('user.home') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Bisnis</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Bisnis
        <small>data seluruh bisnis dikelola</small>
    </h3>
    <div class="row">
        @if($list->count() > 0)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bisnis-thumb" onclick="window.location='{{route('user.bisnis.create')}}'">
                <div class="dashboard-stat blue">
                    <div class="visual">
                        <i class="fa fa-plus-circle"></i>
                    </div>
                    <div class="details">
                        <div class="desc">
                            Tambah Bisnis Baru<br>
                            <span>
                            </span>
                        </div>
                    </div>
                    <a class="more" href="#"> Tambah Bisnis
                        <i class="m-icon-swapright m-icon-black"></i>
                    </a>
                </div>
            </div>
            @foreach($list as $data)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bisnis-thumb" onclick="window.location='{{route('user.bisnis.detail',$data->id)}}'">
                <div class="dashboard-stat grey">
                    <div class="visual">
                        <i class="fa fa-building"></i>
                    </div>
                    <div class="details">
                        <div class="desc">
                            {{$data->name}}<br>
                            <span>
                            @if($data->kategori()->first())
                                {{$data->kategori()->first()->name}}<br>
                            @endif
                            @if($data->subKategori()->first())
                                {{$data->subKategori()->first()->name}}
                            @endif
                            </span>
                        </div>
                    </div>
                    <a class="more" href="#"> Selengkapnya
                        <i class="m-icon-swapright m-icon-black"></i>
                    </a>
                </div>
            </div>
            @endforeach
            <div class="col-md-12">
                <center>
                    {{$list->render()}}                                
                </center>
            </div>
        @else
            <center>
                <span>Anda belum memiliki bisnis untuk dikelola.
                <a href="{{route('user.bisnis.create')}}" class="btn btn-primary">Tambah</a> bisnis baru atau klaim bisnis Anda dalam list.</span>
            </center>
        @endif
    </div>
    <div class="clearfix"></div>
</div>
@endsection

@section('javascript')
@endsection