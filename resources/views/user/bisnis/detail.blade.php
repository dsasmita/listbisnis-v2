@extends('user.layouts.default')

@section('css')
<style type="text/css">
  .gmap img {
    max-width: none !important;
  }
</style>
@endsection

@section('content')
<div class="page-content">
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('front') }}">Beranda</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('user.home') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('user.bisnis') }}">Bisnis</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{$bisnis->name}}</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> {{$bisnis->name}} <a target="_blank" class="btn btn-info" title="lihat bisnis" href="{{ route('lokasi', $bisnis->slug) }}"><span aria-hidden="true" class="icon-share-alt"></span></a>
        <br>
        <small>
            @if($bisnis->kategori()->first())
                {{$bisnis->kategori()->first()->name}},
            @endif
            @if($bisnis->subKategori()->first())
                {{$bisnis->subKategori()->first()->name}}
            @endif
        </small>
    </h3>
    
    <div class="row">
      <form action="#" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-5">
            <div class="block-title clearfix">
                <div class="form-group">
                  <div class="col-md-12" style="padding-left:0px;padding-right:0px">
                      <div class="input-group has-success">
                          <input type="text" id="location" name="address-maps" class="form-control" placeholder="Cari lokasi">
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-success geolocation" title="find my location"><i class="fa fa-map-marker"></i></button>
                          </span>
                      </div>
                  </div>
                </div>
            </div>
            <div id="map" class="gmap"></div>
            <div class="form-group" style="padding-bottom:15px">
                <div class="col-md-6" style="padding-left:0px;padding-right:0px">
                  <input value="{{ old('latitude') == '' ? $bisnis->latitude : old('latitude') }}" type="text" id="latitude" name="latitude" class="form-control" placeholder="*longitude">
                </div>
                <div class="col-md-6" style="padding-left:0px;padding-right:0px">
                  <input value="{{ old('longitude') == '' ? $bisnis->longitude : old('longitude') }}" type="text" id="longitude" name="longitude" class="form-control" placeholder="*latitude">
                </div>
            </div>
        </div>
        <div class="col-md-7">
          <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-diamond font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Detail Bisnis</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#bisnis_info" data-toggle="tab"> Info </a>
                    </li>
                    <li>
                        <a href="#bisnis_alamat" data-toggle="tab"> Alamat </a>
                    </li>
                    <li>
                        <a href="#bisnis_kontak" data-toggle="tab"> Kontak </a>
                    </li>
                    <li>
                        <a href="#bisnis_seo" data-toggle="tab"> SEO </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="bisnis_info">
                        <div class="form-body">
                          <div class="form-group">
                            <label>Nama <span class="font-red-thunderbird">*</span></label>
                            <input type="text" name="name" value="{{ @$update['name'] }}" class="form-control" placeholder="nama bisnis"> 
                          </div>
                          <div class="form-group">
                            <label>Kategori <span class="font-red-thunderbird">*</span></label>
                            <select name="category" id="category" class="form-control">
                              <option value="">-pilih kategori-</option>
                              @foreach($categories as $cat)
                                <option {{$cat->slug == @$update['category'] ? 'selected' : ''}} value="{{$cat->slug}}">{{ $cat->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Sub Kategori</label>
                            <select name="category_sub" id="category_sub" class="form-control">
                              <option value="">-pilih sub kategori-</option>
                              @foreach($categories_sub as $cat)
                                  <option {{$cat->slug == @$update['category_sub'] ? 'selected' : ''}} value="{{$cat->slug}}">{{ $cat->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Deskripsi <span class="font-red-thunderbird">*</span></label>
                            <textarea name="description" placeholder="deskripsi bisnis" class="form-control">{{ @$update['description'] }}</textarea>
                          </div>
                          <div class="form-group">
                            <label>Logo <small>*ukuran maksimal 512kB</small></label>
                            <br>
                            @if($bisnis->logo != '')
                              <img src="{{asset(str_replace($bisnis->logo_filename,'square-'.$bisnis->logo_filename,$bisnis->logo))}}" style="" width="100px">
                            @endif
                            <input type="file" name="file_logo" id="file_logo">
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="bisnis_alamat">
                        <div class="form-body">
                          <div class="form-group">
                            <label>Provinsi <span class="font-red-thunderbird">*</span></label>
                            <select name="provinsi" id="provinsi" class="form-control">
                              <option value="">-pilih provinsi-</option>
                              @foreach($provinsies as $cat)
                                <option {{$cat->slug == @$update['provinsi'] ? 'selected' : ''}} value="{{$cat->slug}}">{{ $cat->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Kabupaten/Kota <span class="font-red-thunderbird">*</span></label>
                            <select name="kabkota" id="kabkota" class="form-control">
                              <option value="">-pilih kabupaten/kota-</option>
                              @foreach($kabkotas as $cat)
                                <option {{$cat->slug == @$update['kabkota'] ? 'selected' : ''}} value="{{$cat->slug}}">{{ $cat->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Kodepos <span class="font-red-thunderbird">*</span></label>
                            <input type="text" maxlength="5" value="{{@$update['kodepos']}}" name="kodepos" class="form-control" placeholder="kodepos"> 
                          </div>
                          <div class="form-group">
                            <label>Alamat <span class="font-red-thunderbird">*</span></label>
                            <textarea name="address" placeholder="alamat bisnis" class="form-control">{{@$update['address']}}</textarea>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="bisnis_kontak">
                        <div class="form-body">
                          <div class="form-group">
                            <label>No Telpon <span class="font-red-thunderbird">*</span></label>
                            <input type="text" name="phone_number" value="{{@$update['phone_number']}}" class="form-control" placeholder="no telpon"> 
                          </div>
                          <div class="form-group">
                            <label>Fax</label>
                            <input type="text" name="fax_number" value="{{@$update['fax_number']}}" class="form-control" placeholder="fax"> 
                          </div>
                          <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="{{@$update['email']}}" class="form-control" placeholder="email"> 
                          </div>
                          <div class="form-group">
                            <label>Website</label>
                            <input type="text" name="website" value="{{@$update['website']}}" class="form-control" placeholder="website"> 
                          </div>
                          <div class="form-group">
                            <label>Facebook Link</label>
                            <input type="text" name="fb_link" value="{{@$update['fb_link']}}" class="form-control" placeholder="link facebook"> 
                          </div>
                          <div class="form-group">
                            <label>Twitter Link</label>
                            <input type="text" name="twitter_link" value="{{@$update['twitter_link']}}" class="form-control" placeholder="link twitter"> 
                          </div>
                          <div class="form-group">
                            <label>Instagram Link</label>
                            <input type="text" name="instagram_link" value="{{@$update['instagram_link']}}" class="form-control" placeholder="link instagram"> 
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="bisnis_seo">
                      <div class="form-body">
                        <div class="form-group">
                          <label>Title</label>
                          <input type="text" value="{{@$update['seo_title']}}" name="seo_title" class="form-control" placeholder="SEO title"> 
                        </div>
                        <div class="form-group">
                          <label>Keyword</label>
                          <input type="text" name="seo_keyword" value="{{@$update['seo_keyword']}}" class="form-control" placeholder="SEO keyword"> 
                        </div>
                        <div class="form-group">
                          <label>Description</label>
                          <textarea name="seo_description" placeholder="SEO description" class="form-control">{{@$update['seo_description']}}</textarea> 
                        </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue btn-block">Simpan</button>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="clearfix"></div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function ($) {
    $('#category').change(function(e){
      var slug = $(this).val();
      $.ajax({
        dataType: "json",
        type: "GET",
        url: '{{route('additional.categoriessub.json')}}?slug='+slug
      }).done(function(result) {
        $('#category_sub option:gt(0)').remove();
        if(result.status == 'OK'){
          $.each(result.list, function(key,value) {
            console.log(value['name']);
            $('#category_sub').append($("<option></option>")
               .attr("value", value['slug']).text(value['name']));
          });
        }
      });
    });

    $('#provinsi').change(function(e){
      var slug = $(this).val();
      $.ajax({
        dataType: "json",
        type: "GET",
        url: '{{route('additional.kabkota.json')}}?slug='+slug
      }).done(function(result) {
        $('#kabkota option:gt(0)').remove();
        if(result.status == 'OK'){
          $.each(result.list, function(key,value) {
            $('#kabkota').append($("<option></option>")
               .attr("value", value['slug']).text(value['name']));
          });
        }
      });
    });
  });
</script>
<!-- Gmaps -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&key={{env('GOOGLE_API')}}&libraries=places"></script>
<script type="text/javascript"> 
    var mapStyles = [];
    $(document).ready(function ($) {
      //google map
      $('.gmap').css('height', '233px');
      
      //make google maps and create markers
      var zoomMap = 14;
      @if($bisnis->latitude != '' AND $bisnis->longitude != '')
        var _latitude = {{$bisnis->latitude}};
        var _longitude = {{$bisnis->longitude}};
      @else
        var _latitude = {{env('DEFAULT_LATITUDE')}};
        var _longitude = {{env('DEFAULT_LONGITUDE')}};
      @endif
          
      var mapCenter = new google.maps.LatLng(_latitude, _longitude);
      var location = new google.maps.LatLng(_latitude, _longitude); 
      var mapOptions = {
          zoom: zoomMap,
          center: mapCenter,
          disableDefaultUI: false,
          scrollwheel: false,
          styles: mapStyles,
          mapTypeControlOptions: {
              style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              position: google.maps.ControlPosition.BOTTOM_CENTER
          },
          panControl: true,
          streetViewControl: true,
          zoomControl: true,
          zoomControlOptions: {
              style: google.maps.ZoomControlStyle.LARGE,
              position: google.maps.ControlPosition.RIGHT_TOP
          }
      };
      var mapElement = document.getElementById('map');
      var map = new google.maps.Map(mapElement, mapOptions);
      
      marker = new google.maps.Marker({
          map:map,
          draggable:false,
          animation: google.maps.Animation.DROP,
          position: location
      });
      
      google.maps.event.addListener(map, 'click', function(a) {
          //console.log('lat'+a.latLng.lat()+',lang'+a.latLng.lng());
          $('#latitude').val(a.latLng.lat());
          $('#longitude').val(a.latLng.lng()); 
      });

      function placeMarker(location) {
          if ( marker ) {
            marker.setPosition(location);
          } else {
            marker = new google.maps.Marker({
              position: location,
              map: map
            });
          }
      }

      google.maps.event.addListener(map, 'click', function(event) {
          placeMarker(event.latLng);
      });

      //autocomplete
      var input = document.getElementById('location') ;
      var autocomplete = new google.maps.places.Autocomplete(input, {
          types: ["geocode"]
      });
      autocomplete.bindTo('bounds', map);
      google.maps.event.addListener(autocomplete, 'place_changed', function() {
          var place = autocomplete.getPlace();
          if (!place.geometry) {
              return;
          }
          if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
              map.setZoom(zoomMap);
          } else {
              map.setCenter(place.geometry.location);
              map.setZoom(zoomMap);
          }

          var address = '';
          if (place.address_components) {
              address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
              ].join(' ');
          }
      });

      //onclick geolocation
      $('.geolocation').on("click", function() {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(success);
          } else {
              console.log('Geo Location is not supported');
          }
      });

      function success(position) {
          var locationCenter = new google.maps.LatLng( position.coords.latitude, position.coords.longitude);
          map.setCenter( locationCenter );
          map.setZoom(zoomMap);

          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({
              "latLng": locationCenter
          }, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  var lat = results[0].geometry.location.lat(),
                      lng = results[0].geometry.location.lng(),
                      placeName = results[0].address_components[0].long_name,
                      latlng = new google.maps.LatLng(lat, lng);

                  $("#location").val(results[0].formatted_address);
              }
          });
      }
    });            
</script>
@endsection