<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ @$title }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- SEO -->
        @include('include.seo')

        @include('user.layouts.css-script')
        <link href=" {{asset('assets-app/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href=" {{asset('assets-app/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href=" {{asset('assets-app/assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href=" {{asset('assets-app/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
        <link href=" {{asset('assets-app/assets/pages/css/login-4.css')}}" rel="stylesheet" type="text/css" />
        @yield('css')
        <link rel="shortcut icon" href="{{asset('assets-app/assets/apps/img/favicon.png')}}" />
    </head>
    <body class="login">
        <div class="logo">
            <a href="{{ route('front') }}" title="List Bisnis">
                <img src="{{asset('assets-app/assets/apps/img/logo2.png')}}" alt="logo" width="75px" />
            </a>
        </div>
        <div class="content">
            @yield('content')
        </div>
        <div class="copyright"> {{date('Y')}} &copy; <a href="{{ route('front') }}">List Bisnis</a> App</div>
        @include('user.layouts.js-script')
        <script src="{{asset('assets-app/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets-app/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets-app/assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets-app/assets/global/plugins/backstretch/jquery.backstretch.min.js')}}" type="text/javascript"></script>
        
        <script src="{{asset('assets-app/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets-app/assets/pages/scripts/login-4.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
            @if(Session::get('notif-error'))
                toastr.error('{{Session::get('notif-error')}}', 'Notifikasi!')
            @endif
            @if(Session::get('notif-warning'))
                toastr.warning('{{Session::get('notif-warning')}}', 'Notifikasi!')
            @endif
            @if(Session::get('notif-success'))
                toastr.success('{{Session::get('notif-success')}}', 'Notifikasi!')
            @endif
            @foreach ($errors->all() as $message)
                toastr.warning('{{$message}}', 'Notifikasi!')
            @endforeach
        </script>
        @yield('javascript')
        @if(env('APP_ENV') == 'production')
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-41538824-1', 'auto');
          ga('send', 'pageview');
        </script>
        @endif
    </body>
</html>