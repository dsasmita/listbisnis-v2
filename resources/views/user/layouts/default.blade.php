<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{@$title}}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        @include('include.seo')
        @include('user.layouts.css-script')
        <link href="{{url('assets-app/assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('assets-app/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets-app/assets/layouts/layout/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets-app/assets/layouts/layout/css/themes/light.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{url('assets-app/assets/layouts/layout/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
        
        <link href="{{url('assets-app/assets/global/css/custom.css')}}" rel="stylesheet" type="text/css" />
        @yield('css')
        <link rel="shortcut icon" href="{{asset('assets-app/assets/apps/img/favicon.png')}}" />
    </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-boxed page-footer-fixed">
        @include('user.layouts.navigation-top')
        <div class="clearfix"> </div>
        <div class="container">
            <div class="page-container">
                @include('user.layouts.navigation-left')
                <div class="page-content-wrapper">
                    @yield('content')
                </div>
            </div>
        </div>
        <div class="page-footer">
            <div class="container">
                <div class="page-footer-inner"> 
                    {{date('Y')}} &copy; <a href="{{ route('front') }}">List Bisnis</a> App
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
        </div>
        @include('user.layouts.js-script')
        <script src="{{url('assets-app/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
        <script src="{{url('assets-app/assets/layouts/layout/scripts/layout.min.js')}}" type="text/javascript"></script>
        <script src="{{url('assets-app/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
            @if(Session::get('notif-error'))
                toastr.error('{{Session::get('notif-error')}}', 'Notifikasi!')
            @endif
            @if(Session::get('notif-warning'))
                toastr.warning('{{Session::get('notif-warning')}}', 'Notifikasi!')
            @endif
            @if(Session::get('notif-success'))
                toastr.success('{{Session::get('notif-success')}}', 'Notifikasi!')
            @endif
            @foreach ($errors->all() as $message)
                toastr.warning('{{$message}}', 'Notifikasi!')
            @endforeach
        </script>
        @yield('javascript')
        @if(env('APP_ENV') == 'production')
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-41538824-1', 'auto');
          ga('send', 'pageview');
        </script>
        @endif
    </body>

</html>