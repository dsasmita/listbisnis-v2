<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler"> </div>
            </li>
            <li class="nav-item start {{ active(['user.home','user.profile']) }}">
                <a href="{{route('user.home')}}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    @if (is_active(['user.home','user.profile']))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Menu</h3>
            </li>
            <li class="nav-item {{ active(['user.bisnis','user.bisnis.detail','user.bisnis.create']) }}">
                <a href="{{route('user.bisnis')}}" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Bisnis</span>
                    @if (is_active(['user.bisnis','user.bisnis.detail','user.bisnis.create']))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item hidden">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-envelope"></i>
                    <span class="title">Kotak Masuk</span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>