<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner container">
        <div class="page-logo">
            <a target="_blank" href="{{ route('front') }}" class="">
                <img src="{{url('assets-app/assets/apps/img/logo.png')}}" height="25px" alt="logo" class="logo-default" style="margin: 13px 0 0;background-color: #fff;"/>
            </a>
            <div class="menu-toggler sidebar-toggler"> </div>
        </div>
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src=" {{url('assets-app/assets/layouts/layout/img/avatar.jpg')}}" />
                        <span class="username username-hide-on-mobile"> {{\Auth::guard('user')->user()->name}} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{ route('user.profile') }}">
                                <i class="icon-user"></i> Profil </a>
                        </li>
                        <li class="hidden">
                            <a href="#">
                                <i class="icon-envelope"></i> Kotak Masuk
                                <span class="badge badge-success"> 7 </span>
                            </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="{{ route('user.logout') }}">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>