@extends('frontend.layouts.app')

@section('content')
<div class="privacy main-grid-border">
    <div class="container">
      <h1 class="head">Sitemap</h1>
      <div class="row" style="padding-left: 70px">
        <div class="col-md-4">
          <h3><a href="{{ route('front') }}">Beranda</a></h3>
          <ul>
            <li><a href="{{ route('front') }}">Beranda</a></li>
            <li><a href="{{ route('kategori.list') }}">Kategori</a></li>
            <li><a href="{{ route('area') }}">Wilayah</a></li>
            <li><a href="{{ route('static.disclaimer') }}">Sangkalan</a></li>
            <li><a href="{{ route('static.kontak') }}">Kontak</a></li>
            <li><a href="{{ route('static.sitemap') }}">Sitemap</a></li>
          </ul>
        </div>
        <div class="col-md-4">
          <h3><a href="{{ route('kategori.list') }}">Kategori</a></h3>
          <ul>
            @foreach(\App\Models\category::categorySelect() as $select)
                <li><a href="{{ route('kategori',$select['category']['slug']) }}">{{$select['category']['name']}}</a></li>
                @foreach($select['child'] as $selectChild)
                  <li> >><a href="{{ route('kategori',$selectChild['slug']) }}">{{$selectChild['name']}}</a></li>
                @endforeach
            @endforeach
          </ul>
        </div>
        <div class="col-md-4">
          <h3><a href="{{ route('area') }}">Wilayah</a></h3>
          <ul>
            @foreach(\App\Models\region::regionSelect() as $select)
              <li><a href="{{ route('pencarian').'?lokasi='.$select['category']['name'] }}">Provinsi {{$select['category']['name']}}</a></li>
              @foreach($select['child'] as $selectChild)
                  <li> >><a href="{{ route('pencarian').'?lokasi='.$selectChild['name'] }}">{{$selectChild['name']}}</a></li>
              @endforeach
            @endforeach
          </ul>
        </div>
      </div>
    </div>  
</div>
@endsection


@section('javascript')
<script type="text/javascript">
</script>
@endsection