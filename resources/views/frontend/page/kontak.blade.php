@extends('frontend.layouts.app')

@section('content')
<div class="faq main-grid-border">
      <div class="container">
            <h1 class="head">Kontak</h1>
            <form class="form-horizontal" method="post" action="#" style="padding:50px;background-color:#eee;">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="form-group">
                    <label for="nama" class="col-md-3 control-label">Nama*</label>
                    <div class="col-md-5">
                        <input type="text" class="form-control" value="{{old('nama')}}" name="nama" id="nama" placeholder="nama">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-3 control-label">Email*</label>
                    <div class="col-md-5">
                        <input type="text" class="form-control" value="{{old('email')}}" name="email" id="email" placeholder="email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="no_hp" class="col-md-3 control-label">No Hp</label>
                    <div class="col-md-5">
                        <input type="text" name="no_hp" class="form-control" value="{{old('no_hp')}}" id="no_hp" placeholder="no hp">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pesan" class="col-md-3 control-label">Pesan*</label>
                    <div class="col-md-7"><textarea name="pesan" id="pesan" class="form-control" placeholder="pesan atau komentar">{{ old('pesan') }}</textarea></div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-7">
                        <button type="submit" class="btn btn-info">Kirim</button>
                    </div>
                </div>
            </form>
      </div>
</div>
@endsection


@section('javascript')
<script type="text/javascript">
</script>
@endsection