@extends('frontend.layouts.app')

@section('content')
<div class="privacy main-grid-border">
    <div class="container">
      <h1 class="head">Sangkalan / Disclaimer</h1>
      <p>
      List Bisnis hanya menampilkan data atau 
      informasi berkaitan dengan hal tersebut yang dengan 
      cara tertentu telah menjadi domain publik.
      </p>
      <p>
      List Bisnis tidak bertanggung-jawab atas tidak 
      tersampaikannya data/informasi yang disampaikan 
      oleh pembaca melalui berbagai jenis saluran 
      komunikasi (e-mail, sms, online form) karena 
      faktor kesalahan teknis yang tidak diduga-duga 
      sebelumnya. 
      </p>
      <p>
      List Bisnis berhak untuk memuat , tidak memuat, 
      mengedit, dan/atau menghapus data/informasi yang 
      disampaikan oleh pembaca.
      </p>
      <p>
      Data dan/atau informasi yang tersedia di List Bisnis 
      hanya sebagai rujukan/referensi belaka, dan tidak 
      diharapkan untuk tujuan perdagangan saham, transaksi 
      keuangan/bisnis maupun transaksi lainnya.
      </p>
    </div>  
</div>
@endsection


@section('javascript')
<script type="text/javascript">
</script>
@endsection