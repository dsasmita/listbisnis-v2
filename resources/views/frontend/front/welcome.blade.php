@extends('frontend.layouts.app')

@section('content')
<div class="categories">
    <div class="container">
        @if(env('APP_ENV') == 'production')
        <div class="col-md-12">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- LB: Beranda -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-0124899135832986"
                 data-ad-slot="4366206037"
                 data-ad-format="auto"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
            <!-- <center>
            <a href="https://bonakma.com" target="_blank">
                <img src="{{asset('assets/images/iklan-bonakma.png')}}" alt="Bonakma.com">
            </a>
            </center> -->
        </div>
        @endif
        @foreach($categories as $cat)
        <div class="col-md-2 focus-grid">
            <a href="{{ route('kategori',$cat->slug) }}">
                <div class="focus-border">
                    <div class="focus-layout">
                        <h3>{{number_format($cat->count)}}</h3>
                        <h4 class="clrchg">{{$cat->name}}</h4>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
        <div class="clearfix"></div>
    </div>
</div>
<div class="mobile-app">
    <div class="container">
        <div class="col-md-5 app-left">
            <a href="javascript:void(0)"><img src="{{asset('assets/images/app.png')}}" alt=""></a>
        </div>
        <div class="col-md-7 app-right">
            <h3>Kini Informasi Bisnis di Jemari Anda</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam auctor Sed bibendum varius euismod. Integer eget turpis sit amet lorem rutrum ullamcorper sed sed dui. vestibulum odio at elementum. Suspendisse et condimentum nibh.</p>
            <div class="app-buttons">
                <div class="app-button">
                    <a href="#"><img src="{{asset('assets/images/1.png')}}" alt=""></a>
                </div>
                <div class="app-button">
                    <a href="#"><img src="{{asset('assets/images/2.png')}}" alt=""></a>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@endsection
