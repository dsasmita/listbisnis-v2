@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <h2 class="head">Kategori</h2>
</div>
<div class="sitemap-regions">
    <div class="container">
        @foreach(\App\Models\category::categorySelect() as $kategori)
        <div class="sitemap-region-grid">
            <div class="sitemap-region {{$kategori['category']['slug']}}">
                <h4>{{$kategori['category']['name']}}</h4>
                <ul>
                    <?php
                    $iterate = 0;
                    ?>
                    <li class=""><a href="{{ route('kategori',$kategori['category']['slug']) }}"><strong>{{$kategori['category']['name']}} ({{ number_format($kategori['category']['count']) }})</strong></a></li>
                    @foreach($kategori['child'] as $selectChild)
                        <li class="{{ ($iterate++ >= 5) ? 'more-sub hidden' : '' }}"><a href="{{ route('kategori',$selectChild['slug']) }}">{{$selectChild['name']}} ({{$selectChild['count']}})</a></li>
                    @endforeach
                    @if($iterate >= 5)
                        <li class="left-gap "><a href="javascript:void(0)" class="more" data-slug="{{$kategori['category']['slug']}}" style="color:#f3c500; text-transform:none">selengkapnya...</a></li>
                    @endif
                </ul>
            </div>
        </div>
        @endforeach
        <div class="clearfix"></div>
    </div>
</div>
@endsection


@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
    $('.more').click(function(e){
        e.preventDefault();
        var slug = $(this).data('slug');
        var check = $("."+slug+" .more-sub").hasClass("hidden");
        if(check){
            $("."+slug+" .more-sub").removeClass("hidden");
            $(this).text('minimal...');
        }else{
            $("."+slug+" .more-sub").addClass("hidden");
            $(this).text('selengkapnya...');
        }
    });
});
</script>
@endsection