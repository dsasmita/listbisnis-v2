@extends('frontend.layouts.app')

@section('css')
<style type="text/css">
.interested p {
    color: #000;
    font-size: 14px;
    margin: 10px 0 0 0;
}
</style>
@endsection

@section('content')
<div class="container">
    <ol class="breadcrumb" style="margin-bottom: 5px;margin-top:20px">
        <li><a href="{{ route('front') }}">Beranda</a></li>
        <li><a href="{{ route('kategori',$kategori->slug) }}">{{$kategori->name}}</a></li>
        <li><a href="{{ route('kategori',$subKategori->slug) }}">{{$subKategori->name}}</a></li>
        <li class="active">{{ $lokasi->name }}</li>
    </ol>
    <div class="product-desc">
        <div class="col-md-7 product-view">
            <h1>{{$lokasi->name}}</h1>
            @if(env('APP_ENV') == 'production')
            <div>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- LB: Beranda -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-0124899135832986"
                     data-ad-slot="4366206037"
                     data-ad-format="auto"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            @endif
            @if($lokasi->logo)
                <img class="pull-right" style="width:100px" title="{{ $lokasi->name }}" alt="{{ $lokasi->name }}" src="{{ asset($lokasi->logo) }}">
            @endif
            <p> <i class="glyphicon glyphicon-eye-open"></i> {{number_format($lokasi->views)}} views | <i class="glyphicon glyphicon-comment"></i> {{number_format($lokasi->pesan_counter)}} comment</p>
            <div class="addthis_inline_share_toolbox"></div>
            <div class="product-details">
                <h4>Kategori : <a href="{{ route('kategori',$kategori->slug) }}">{{$kategori->name}}</a></h4>
                <h4>Sub Kategori : <a href="{{ route('kategori',$subKategori->slug) }}">{{$subKategori->name}}</a></h4>
                <p>
                    <strong>Alamat</strong>: {{ $lokasi->address }}
                    @if($lokasi->city()->first())
                        - <a href="{{ route('area.detail',$lokasi->city()->first()->slug) }}">{{trim($lokasi->city()->first()->name)}}</a>,
                    @endif
                    @if($lokasi->province()->first())
                        <a href="{{ route('area.detail',$lokasi->province()->first()->slug) }}">{{$lokasi->province()->first()->name}}</a>
                    @endif
                    {{ $lokasi->kodepos }}
                </p>
                <p><strong>Deskripsi:</strong>
                @if($lokasi->description != '')
                    {{$lokasi->description}}
                @else
                    Bisnis {{$lokasi->name}} adalah bisnis yang bergerak di bidang {{$kategori->name}}
                    yang khusus menangani bisnis {{$subKategori->name}}, beralamat di {{$lokasi->address}}.
                @endif
                </p>
                @if($lokasi->latitude != 0 || $lokasi->latitude != '')
                    <div id="map" style="height:200px;width:100%"></div>
                    <script>
                    function initMap() {
                      var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 14,
                        center: {lat: {{$lokasi->latitude}}, lng: {{$lokasi->longitude}}}
                      });

                      var image = '{{ asset('assets/images/pointer.png') }}';
                      var beachMarker = new google.maps.Marker({
                        position: {lat: {{$lokasi->latitude}}, lng: {{$lokasi->longitude}} },
                        map: map,
                        icon: image
                      });
                    }

                    </script>
                    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API')}}&signed_in=true&callback=initMap"></script>
                @endif
                <hr>
                <div class="" id="komenRating">
                    <h2>Form Feedback & Komentar</h2>
                    <div class="">
                        <form class="form-horizontal" method="post">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <label for="nama" class="col-md-3 control-label">Nama*</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" value="{{old('nama')}}" name="nama" id="nama" placeholder="nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-md-3 control-label">Email*</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" value="{{old('email')}}" name="email" id="email" placeholder="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="no_hp" class="col-md-3 control-label">No Hp</label>
                                <div class="col-md-5">
                                    <input type="text" name="no_hp" class="form-control" value="{{old('no_hp')}}" id="no_hp" placeholder="no hp">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="radio" class="col-md-3 control-label">Rating</label>
                                <div class="col-md-9">
                                    <div class="radio-inline"><label><input type="radio" name="star" value="5" {{old('star') == 5 ? 'checked' : ''}}> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></label></div>
                                    <div class="radio-inline"><label><input type="radio" name="star" value="4" {{old('star') == 4 ? 'checked' : ''}}> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></label></div>
                                    <div class="radio-inline"><label><input type="radio" name="star" value="3" {{old('star') == 3 ? 'checked' : ''}}> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></label></div>
                                    <div class="radio-inline"><label><input type="radio" name="star" value="2" {{old('star') == 2 ? 'checked' : ''}}> <i class="fa fa-star"></i><i class="fa fa-star"></i></label></div>
                                    <div class="radio-inline"><label><input type="radio" name="star" value="1" {{old('star') == 1 ? 'checked' : ''}}> <i class="fa fa-star"></i></label></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pesan" class="col-md-3 control-label">Pesan*</label>
                                <div class="col-md-7"><textarea name="pesan" id="pesan" class="form-control" placeholder="pesan atau komentar">{{ old('pesan') }}</textarea></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-7">
                                    <button type="submit" class="btn btn-info">Kirim</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <h2>Feedback & Komentar</h2>
                    @if(count($pesan) > 0)
                    @foreach($pesan as $psn)
                    <div class="well">
                        <strong>{{ $psn->nama }}: </strong>
                        <small>{{$psn->pesan}}</small><br>
                        <span class=""><small>{{ date('yM d H:i',strtotime($psn->created_at)) }}</small></span>
                    </div>
                    @endforeach
                    @else
                    <div class="well">
                        Tidak ada komentar untuk <strong>{{$lokasi->name}}</strong>, berikan komentar dan rating untuk
                        membantu pengunjung lain mendapatkan referensi.
                    </div>
                    @endif
                </div>
            </div>   
        </div>
        <div class="col-md-5 product-details-grid">
            <div class="item-price">
                <div class="product-price">
                    <p class="p-price">Rating</p>
                    <h3 class="rate rate-star">
                        @for($i = 0; $i < $star['full']; $i++)
                            <i class="fa fa-star"></i>
                        @endfor
                        @for($i = 0; $i < $star['half']; $i++)
                            <i class="fa fa-star-half-empty"></i>
                        @endfor
                        @for($i = 0; $i < $star['zero']; $i++)
                            <i class="fa fa-star-o"></i>
                        @endfor
                    </h3>
                    <div class="clearfix"></div>
                </div>
                <div class="condition">
                    <p class="p-price">Jumlah Rating</p>
                    <h4>{{ number_format($lokasi->star_counter) }}</h4>
                    <div class="clearfix"></div>
                </div>
                <div class="itemtype" style="padding:17px 0">
                    <a href="#komenRating" class="btn btn-info btn-block">Rating</a>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="interested text-center">
                <h4>Kontak<small> Kontak bisnis!</small></h4>
                @if($lokasi->phone_number != '')
                    <p><i class="glyphicon glyphicon-earphone"></i><a href="tel:{{$lokasi->phone_number}}">{{$lokasi->phone_number}}</a></p>
                @endif
                @if($lokasi->fax_number != '')
                    <p><i class="glyphicon glyphicon-print"></i>{{$lokasi->fax_number}}</p>
                @endif
                @if($lokasi->email != '')
                    <p><i class="glyphicon glyphicon-send"></i><a href="mailto:{{$lokasi->email}}">{{$lokasi->email}}</a></p>
                @endif
                @if($lokasi->website != '')
                    <p><i class="glyphicon glyphicon-globe"></i><a target="_blank" href="{{$lokasi->website}}">{{str_replace(['https://','http://'],'',$lokasi->website)}}</a></p>
                @endif
                @if($lokasi->fb_link != '')
                    <p><i class="fa fa-facebook-square"></i><a target="_blank" href="{{$lokasi->fb_link}}">{{str_replace(['https://','http://'],'',$lokasi->fb_link)}}</a></p>
                @endif
                @if($lokasi->twitter_link != '')
                    <p><i class="fa fa-twitter-square"></i><a target="_blank" href="{{$lokasi->twitter_link}}">{{str_replace(['https://','http://'],'',$lokasi->twitter_link)}}</a></p>
                @endif
                @if($lokasi->instagram_link != '')
                    <p><i class="fa fa-twitter-square"></i><a target="_blank" href="{{$lokasi->instagram_link}}">{{str_replace(['https://','http://'],'',$lokasi->instagram_link)}}</a></p>
                @endif
            </div>
            @if($lokasi->claim_by == '')
            <div class="interested text-center">
                <h4>Apakah anda pemilik alamat ini?</h4>
                <p><a href="{{route('claim.bisnis',$lokasi->id)}}" class="btn btn-default" onclick="return confirm('Apakah anda pemilik bisnis ini, dan akan mengelola informasi bisnis ini?')"><strong>Klaim sekarang</strong></a></p>
            </div>
            @endif
            <!-- <div class="interested text-center">
                <script type="text/javascript">
                  ( function() {
                    if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; };
                    var unit = {"calltype":"async[2]","publisher":"dsasmita","width":300,"height":250,"sid":"Chitika Default"};
                    var placement_id = window.CHITIKA.units.length;
                    window.CHITIKA.units.push(unit);
                    document.write('<div id="chitikaAdBlock-' + placement_id + '"></div>');
                }());
                </script>
                <script type="text/javascript" src="//cdn.chitika.net/getads.js" async></script>
            </div>

            <div class="interested text-center">
                <a href="https://bonakma.com" target="_blank">
                    <img src="{{asset('assets/images/iklan-bonakma.png')}}" alt="Bonakma.com">
                </a>
            </div> -->
            
        </div>
    <div class="clearfix"></div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58242b80af5e632e"></script> 
@endsection