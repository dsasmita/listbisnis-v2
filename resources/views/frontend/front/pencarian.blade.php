@extends('frontend.layouts.app')

@section('content')
<div class="total-ads main-grid-border">
    <div class="container">
        @if(@$categoryChild)
        <div class="all-categories">
            <h3> Pilihan kategori terkait {{$category->name}}</h3>
            <ul class="all-cat-list">
                @foreach($categoryChild as $child)
                    <li><a href="{{route('kategori',$child->slug)}}">{{$child->name}} <!-- <span class="num-of-ads">(5,78,076)</span> --></a></li>
                @endforeach
            </ul>
        </div>
        @endif
        <ol class="breadcrumb" style="margin-bottom: 5px;margin-top:10px;">
            <li><a href="{{ route('front') }}">Beranda</a></li>
            <li class="active">Hasil Pencarian</li>
        </ol>
        <div class="ads-grid">
            <div class="side-bar col-md-3">
                <div class="featured-ads">
                    <h2 class="sear-head fer">Featured Ads</h2>
                    @if(env('APP_ENV') == 'production')
                    <!-- <div class="featured-ad">
                        <a href="http://listbisnis.com/bisnis/bonakma">
                            <div class="featured-ad-left">
                                <img src="http://listbisnis.com/uploads/2016/11/22/square-logo-1.jpg" title="ad image" alt="" />
                            </div>
                            <div class="featured-ad-right">
                                <h4>BonAkma - Abon Ayam Premium, cocok untuk semua usia</h4>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </div> -->

                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- LB: Beranda -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-0124899135832986"
                         data-ad-slot="4366206037"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    
                    @endif
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="ads-display col-md-9">
                <div class="wrapper">
                    <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                                <div>
                                    <div id="container">
                                        <h1 class="sear-head">{{ $title_html }}</h3>
                                        <hr>
                                        <div class="sort hidden">
                                            <div class="sort-by pull-right">
                                                <label>Sort By : </label>
                                                <select>
                                                    <option value="">Most recent</option>
                                                    <option value="">Price: Rs Low to High</option>
                                                    <option value="">Price: Rs High to Low</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <ul class="list">
                                            @foreach(@$list as $data)
                                                <li>
                                                    <a href="{{ route('lokasi',$data->slug) }}">
                                                    @if($data->logo != '')
                                                        <img src="{{asset($data->logo)}}" title="{{ $data->name }}" alt="{{ $data->name }}" />
                                                    @else
                                                        <img src="{{asset('assets/images/logo-sekitr.png')}}" title="{{ $data->name }}" alt="default logo" />
                                                    @endif
                                                    </a>
                                                    <section class="list-left">
                                                        <h5 class="title"><a href="{{ route('lokasi',$data->slug) }}">{{$data->name}}</a></h5>
                                                        <span class="address">{{$data->address}}</span>
                                                        
                                                        <p class="catpath">
                                                            @if($data->kategori()->first())
                                                                <a href="{{route('kategori',$data->kategori()->first()->slug)}}">{{$data->kategori()->first()->name}}</a> >>
                                                            @endif
                                                            @if($data->subKategori()->first())
                                                                <a href="{{route('kategori',$data->subKategori()->first()->slug)}}">{{$data->subKategori()->first()->name}}</a>
                                                            @endif
                                                        </p>
                                                    </section>
                                                    <section class="list-right">
                                                        <span class="date"><a href="{{ route('lokasi',$data->slug) }}#komenRating" class="btn btn-info btn-small"><i class="fa fa-fw fa-pencil"></i> review</a></span>
                                                        <span class="cityname">{{ number_format($data->star,1) }} <i class="fa fa-star"></i></span>
                                                    </section>
                                                    <div class="clearfix"></div>
                                                </li>
                                            @endforeach
                                            @if(count($list) == 0 )
                                            <li>
                                                {{$title_html}} tidak ditemukan, silahkan dapatkan informasi kategori lainnya
                                                di <a href="{{ route('kategori.list') }}">kategori Bisnis</a>
                                                Direktori Bisnis Indonesia dan Temukan Peluang Penawaran Kerjasama Bisnis dan Usaha di Indonesia
                                            </li>
                                            @else
                                            <li>
                                                {{number_format($list->count()).' from '. number_format($list->total()).' data'}}
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <center>
                                {{$list->appends($search)->render()}}                                
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection
