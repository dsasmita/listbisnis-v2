@extends('frontend.layouts.app')

@section('content')
<div class="total-ads main-grid-border">
    <div class="container">
        @if(@$regionChild)
        <div class="all-categories">
            <h3> Pilihan wilayah terkait {{$region->name}}</h3>
            <div style="margin-left:10px" class="addthis_inline_share_toolbox"></div>
            <ul class="all-cat-list">
                @foreach($regionChild as $child)
                    <li><a href="{{route('area.detail',$child->slug)}}">{{$child->name}} <!-- <span class="num-of-ads">(5,78,076)</span> --></a></li>
                @endforeach
            </ul>
        </div>
        @endif
        <ol class="breadcrumb" style="margin-bottom: 5px;">
            <li><a href="{{ route('front') }}">Beranda</a></li>
            <li class="active">{{$region->name}}</li>
        </ol>
        <div class="ads-grid">
            <div class="side-bar col-md-3">
                <div class="featured-ads">
                    <h2 class="sear-head fer">Featured Ads</h2>
                    <div class="featured-ad">
                        <a href="#">
                            <div class="featured-ad-left">
                                <img src="{{ asset('assets/images/logo-sekitr.png') }}" title="ad image" alt="" />
                            </div>
                            <div class="featured-ad-right">
                                <h4>Lorem Ipsum is simply dummy text of the printing industry</h4>
                                <p class="hidden">$ 450</p>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="ads-display col-md-9">
                <div class="wrapper">
                    <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                                <div>
                                    <div id="container">
                                        <h1 class="sear-head">List Bisnis Kategori {{$region->name}}</h3>
                                        <hr>
                                        <div class="sort hidden">
                                            <div class="sort-by pull-right">
                                                <label>Sort By : </label>
                                                <select>
                                                    <option value="">Most recent</option>
                                                    <option value="">Price: Rs Low to High</option>
                                                    <option value="">Price: Rs High to Low</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <ul class="list">
                                            @foreach(@$list as $data)
                                                <li>
                                                    <a href="{{ route('lokasi',$data->slug) }}">
                                                    @if($data->logo != '')
                                                        <img src="{{asset($data->logo)}}" title="{{ $data->name }}" alt="{{ $data->name }}" />
                                                    @else
                                                        <img src="{{asset('assets/images/logo-sekitr.png')}}" title="{{ $data->name }}" alt="default logo" />
                                                    @endif
                                                    </a>
                                                    <section class="list-left">
                                                        <h5 class="title"><a href="{{ route('lokasi',$data->slug) }}">{{$data->name}}</a></h5>
                                                        <span class="address">{{$data->address}}</span>
                                                        
                                                        <p class="catpath">
                                                            @if($data->kategori()->first())
                                                                <a href="{{route('kategori',$data->kategori()->first()->slug)}}">{{$data->kategori()->first()->name}}</a> >>
                                                            @endif
                                                            @if($data->subKategori()->first())
                                                                <a href="{{route('kategori',$data->subKategori()->first()->slug)}}">{{$data->subKategori()->first()->name}}</a>
                                                            @endif
                                                        </p>
                                                    </section>
                                                    <section class="list-right">
                                                        <span class="date"><a href="{{ route('lokasi',$data->slug) }}#komenRating" class="btn btn-info btn-small"><i class="fa fa-fw fa-pencil"></i> review</a></span>
                                                        <span class="cityname">{{ number_format($data->star,1) }} <i class="fa fa-star"></i></span>
                                                    </section>
                                                    <div class="clearfix"></div>
                                                </li>
                                            @endforeach
                                            @if(count($list) == 0 )
                                            <li>
                                                List bisnis untuk {{$region->name}} tidak ditemukan, silahkan dapatkan informasi wilayah lainnya
                                                di <a href="{{ route('area') }}">wilayah Bisnis</a>
                                                Direktori Bisnis Indonesia dan Temukan Peluang Penawaran Kerjasama Bisnis dan Usaha di Indonesia
                                            </li>
                                            @else
                                            <li>
                                                {{number_format($list->count()).' from '. number_format($list->total()).' data'}}
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <center>
                                {{$list->render()}}                                
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58242b80af5e632e"></script> 
@endsection