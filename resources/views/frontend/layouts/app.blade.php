<!DOCTYPE html>
<html>
<head>
    <title>{{ @$title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('include.seo')

    <link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href="{{asset('assets/css/jquery.uls.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/jquery.uls.grid.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/jquery.uls.lcd.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-select.css')}}">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('assets/css/flexslider.css')}}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet"/>
    <script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/c9b5dc377f4dcb30e84c8418c96d4fa4_1.js" async></script>
    @yield('css')
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="logo">
                <a href="{{ route('front') }}"><span>List</span>Bisnis</a>
            </div>
            <div class="header-right">
                @if(Auth::guard('user')->check())
                    <a class="account" href="{{route('user.home')}}">Dashboard</a>
                @else
                    <a class="account" href="{{route('user.login')}}">Login</a>
                @endif
                <span class="active uls-trigger"><a href="{{ route('kategori.list') }}" style="color:#01a185">Kategori</a></span>
                <span class="active uls-trigger"><a href="{{ route('area') }}" style="color:#01a185">Wilayah</a></span>
            </div>
        </div>
    </div>
    <div class="banner text-center">
      <div class="container">
            <h1>Pusat Informasi Bisnis Indonesia</h1>
            <p>Direktori Bisnis Indonesia dan Temukan Peluang Penawaran Kerjasama Bisnis dan Usaha di Indonesia</p>
            <div class="select-box">
                <form action="{{ route('pencarian') }}" method="get">
                    <div class="select-city-for-local-ads ads-list">
                        <label>Pilih lokasi anda</label>
                        <select name="lokasi" class="selectpicker" data-live-search="true">
                            <option value="">Semua Provinsi</option>
                            @foreach(\App\Models\region::regionSelect() as $select)
                                <optgroup label="{{$select['category']['name']}}">
                                    <option {{@$pencarian['lokasi'] == $select['category']['name'] ? 'selected' : ''}} value="{{$select['category']['name']}}">Provinsi {{$select['category']['name']}}</option>
                                    @foreach($select['child'] as $selectChild)
                                        <option {{@$pencarian['lokasi'] == $selectChild['name'] ? 'selected' : ''}} value="{{$selectChild['name']}}">{{$selectChild['name']}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="browse-category ads-list">
                        <label>Pilih Kategori</label>
                        <select name="kategori" class="selectpicker" data-live-search="true">
                            <option value="">Semua Kategori</option>
                            @foreach(\App\Models\category::categorySelect() as $select)
                                <optgroup label="{{$select['category']['name']}}">
                                    <option {{@$pencarian['kategori'] == $select['category']['slug'] ? 'selected' : ''}} value="{{$select['category']['slug']}}">Semua {{$select['category']['name']}}</option>
                                    @foreach($select['child'] as $selectChild)
                                        <option {{@$pencarian['kategori'] == $selectChild['slug'] ? 'selected' : ''}} value="{{$selectChild['slug']}}">{{$selectChild['name']}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="search-product ads-list">
                        <label>Cari Berdasarkan Nama</label>
                        <div class="search">
                            <div id="custom-search-input">
                            <div class="input-group">
                                <input type="text" name="nama" value="{{@$pencarian['nama']}}" class="form-control input-lg" placeholder="nama tempat" />
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-lg" type="submit">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
      </div>
    </div>
    
    <div class="content">
        @if(Session::get('notif-success'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> {!!Session::get('notif-success')!!}
        </div>
        @endif
        @if(Session::get('notif-error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> {!!Session::get('notif-error')!!}
        </div>
        @endif

        @foreach ($errors->all() as $message)
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Warning!</strong> {{$message}}
        </div>
        @endforeach
        @yield('content')
    </div>
    
    <footer>
        <div class="footer-top">
            <div class="container">
                <div class="foo-grids">
                    <div class="col-md-3 footer-grid">
                        <h4 class="footer-head">List Bisnis</h4>
                        <p>Direktori Bisnis Indonesia dan Temukan Peluang Penawaran Kerjasama Bisnis dan Usaha di Indonesia</p>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h4 class="footer-head">Bantuan</h4>
                        <ul>
                            <li class="hidden"><a href="#">Cara Kerja</a></li>                     
                            <li><a href="{{ route('static.sitemap') }}">Sitemap</a></li>
                            <li class="hidden"><a href="javascript:void(0)">FAQ</a></li>
                            <li class="hidden"><a href="javascript:void(0)">Kritik & Saran</a></li>
                            <li class=""><a href="https://prayertimes.pro" target="_blank">Prayertimes.pro</a></li>
                            <li class=""><a href="http://bonakma.com" target="_blank">Bonakma.com</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h4 class="footer-head">Informasi</h4>
                        <ul>
                            <li><a href="{{ route('static.kontak') }}">Kontak</a></li>   
                            <li class="hidden"><a href="javascript:void(0)">Syarat & Ketentuan</a></li>
                            <li class="hidden"><a href="javascript:void(0)">Kebijakan & Privasi</a></li>  
                            <li><a href="{{ route('static.disclaimer')}}">Sangkalan</a></li>  
                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid">
                        <h4 class="footer-head">Kontak Kami</h4>
                        <address>
                            <ul class="location">
                                <li><span class="glyphicon glyphicon-map-marker"></span></li>
                                <li>Surabaya, Indonesia</li>
                                <div class="clearfix"></div>
                            </ul>   
                            <ul class="location hidden">
                                <li><span class="glyphicon glyphicon-earphone"></span></li>
                                <li></li>
                                <div class="clearfix"></div>
                            </ul>   
                            <ul class="location">
                                <li><span class="glyphicon glyphicon-envelope"></span></li>
                                <li><a href="mailto:sekitr@gmail.com">listbisnis@gmail.com</a></li>
                                <div class="clearfix"></div>
                            </ul>                       
                        </address>
                    </div>
                    <div class="clearfix"></div>
                </div>                      
            </div>  
        </div>  
        <div class="footer-bottom text-center">
        <div class="container">
            <div class="footer-logo">
                <a href="{{ route('front') }}"><span>List</span>Bisnis</a>
            </div>
            <div class="footer-social-icons">
                <ul>
                    <li><a class="facebook" href="https://www.facebook.com/listbisnis"><span>Facebook</span></a></li>
                    <li><a class="twitter" href="https://twitter.com/ListBisnis"><span>Twitter</span></a></li>
                    <li><a class="googleplus" href="https://plus.google.com/+ListBisnis"><span>Google+</span></a></li>
                </ul>
            </div>
            <div class="copyrights">
                <p> &copy; {{date('Y')}} List Bisnis. All Rights Reserved | Design by  <a href="http://w3layouts.com/"> W3layouts</a></p>
            </div>
            <div class="clearfix"></div>
        </div>
        </div>
    </footer>
    <div id="footer-ads">
        <a href="#" class="pull-right">&times; close</a>
        <center>
            @if(env('APP_ENV') == 'production')
            <!-- LB: Beranda -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-0124899135832986"
                 data-ad-slot="4366206037"
                 data-ad-format="auto"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
            @else
            <h1>Iklan</h1>
            @endif
        </center>
    </div>
    <script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/jquery.leanModal.min.js')}}"></script>

    <script src="{{asset('assets/js/jquery.uls.data.js')}}"></script>
    <script src="{{asset('assets/js/jquery.uls.data.utils.js')}}"></script>
    <script src="{{asset('assets/js/jquery.uls.lcd.js')}}"></script>
    <script src="{{asset('assets/js/jquery.uls.languagefilter.js')}}"></script>
    <script src="{{asset('assets/js/jquery.uls.regionfilter.js')}}"></script>
    <script src="{{asset('assets/js/jquery.uls.core.js')}}"></script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    @yield('javascript')
    @if(env('APP_ENV') == 'production')
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-41538824-1', 'auto');
      ga('send', 'pageview');
    </script>
    <style type="text/css">
        #footer-ads{
            position: fixed;
            bottom: 50px;
            width: 80%;
        }
        #footer-ads a{
            font-size: 25px;
        }
    </style>
    <script type="text/javascript">
         $(document).ready(function(){
            $('#footer-ads a').click(function(e){
                e.preventDefault();
                $(this).parents('#footer-ads').remove();
            });
         });
    </script>
    <!-- Hotjar Tracking Code for https://listbisnis.com -->
    <!-- <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:428101,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script> -->
    @endif
</body>
</html>