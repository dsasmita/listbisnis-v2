<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler"> </div>
            </li>
            <li class="sidebar-search-wrapper">
                <form class="sidebar-search  " action="#" method="POST">
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit">
                                <i class="icon-magnifier"></i>
                            </a>
                        </span>
                    </div>
                </form>
            </li>
            <li class="nav-item {{ active('admin.home') }}">
                <a href="{{ route('admin.home') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    @if (is_active('admin.home'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Master Data</h3>
            </li>
            <li class="nav-item {{ active('admin.categories') }}">
                <a href="{{ route('admin.categories') }}" class="nav-link nav-toggle">
                    <i class="fa fa-bars"></i>
                    <span class="title">Kategori</span>
                    @if (is_active('admin.categories'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Pencarian</h3>
            </li>
            <li class="nav-item {{ active('admin.pencarian') }}">
                <a href="{{ route('admin.pencarian') }}" class="nav-link nav-toggle">
                    <i class="fa fa-bars"></i>
                    <span class="title">Pencarian</span>
                    @if (is_active('admin.pencarian'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Bisnis</h3>
            </li>
            <li class="nav-item {{ active('admin.bisnis') }}">
                <a href="{{ route('admin.bisnis') }}" class="nav-link nav-toggle">
                    <i class="fa fa-bars"></i>
                    <span class="title">Bisnis</span>
                    @if (is_active('admin.bisnis'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>