@extends('admin.layouts.default')

@section('title') List Bisnis Indonesia @endsection

@section('seo')
@endsection

@section('author')
@endsection

@section('css')
@endsection

@section('content')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('admin.home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Pencarian</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Pencarian
        <small>Daftar seluruh history pencarian</small>
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bars"></i>Daftar List Pencarian
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content">
                            <tr>
                                <th width="20%"> Query String </th>
                                <th class="numeric"> Views </th>
                                <th> Aksi </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list as $data)
                            <tr>
                                <td> {{$data->query_string}} </td>
                                <td class="numeric"> {{number_format($data->count)}} </td>
                                <td> <a href="#" class="btn btn-xs btn-default">Button</a> </td>
                            </tr>
                            @endforeach
                            @if(count($list) > 0)
                            <tr>
                                <td colspan="3"><em>{{number_format($list->count()).' from '. number_format($list->total()).' data'}}</em></td>
                            </tr>
                            @else
                            <tr>
                                <td colspan="3"> -empty- </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <center>
                        {{$list->render()}}                                
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD STATS 1-->
</div>
@endsection

@section('javascript')
@endsection