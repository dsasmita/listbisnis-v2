@if(env('APP_ENV') != 'production')
    <meta name="robots" content="noindex,nofollow">
    <meta name="googlebot" content="noindex,nofollow" />
@else
    <meta name="robots" content="index,follow">
    <meta name="googlebot" content="index,follow" />
    <meta name="msvalidate.01" content="B875504F6A754D3FE80059F6999A771E" />
@endif
<link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="List Bisnis">
<meta property='og:site_name' content='List Bisnis' />
<meta property="og:title" content="{{ @$seo['title'] }}" />
<meta property="og:description" content="{{ @$seo['description'] }}" />
<meta property="og:url" content="{{ @$seo['url'] }}" />
<meta property="og:image" content="{{ @$seo['image'] }}" />

<meta name="keywords" content="{{ @$seo['keywords'] }}" />
<meta name="description" content="{{@$seo['description']}}" />
<meta name="copyright" content="Copyright List Bisnis" />
<meta name="language" content="id" />
<meta name="distribution" content="Global" />
<meta name="rating" content="General" />
<meta name="expires" content="never" />
<link rel="canonical" href="{{ @$seo['url'] }}"/>