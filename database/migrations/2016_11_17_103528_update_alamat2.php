<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAlamat2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alamat', function($table) {
            $table->integer('pesan_counter');
            $table->integer('star');
            $table->integer('star_counter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alamat', function ($table) {
            $table->dropColumn('pesan_counter');
            $table->dropColumn('star');
            $table->dropColumn('star_counter');
        });
    }
}
