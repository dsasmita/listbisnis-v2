<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commentRating', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_lokasi');
            $table->string('nama');
            $table->string('email');
            $table->string('no_hp')->nullable();
            $table->string('pesan')->nullable();
            $table->integer('star')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commentRating');
    }
}
