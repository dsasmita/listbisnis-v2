<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '248722545542179',
        'client_secret' => 'a832e8aadd4d11604eabf6368b89030e',
        'redirect' => env('FACEBOOK_CALLBACK'),
    ],

    'twitter' => [
        'client_id' => '1F2OrfymyXPFzigSYs5ygMTog',
        'client_secret' => '2ukl4AZxifFEQ52D35DEeUhTC0BgVvY8vMOi06v8EyF20VZLcz',
        'redirect' => env('TWITTER_CALLBACK'),
    ],

    'google' => [
        'client_id' => '772839120865-hoav94gtg02hdkq9u6mocc087m4eu6ni.apps.googleusercontent.com',
        'client_secret' => 'cE74pPl8V1bTEx0NEynE661k',
        'redirect' => env('GOOGLE_CALLBACK'),
    ],
];
